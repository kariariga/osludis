//
//  GameScene.h
//  teste Drag and Drop
//

//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Card.h"
#import "Slot.h"
#import "Letra.h"

@interface GameScene : SKScene

@property bool storyDone;

@property AVAudioPlayer *theme;

@property int estado;

@property SKSpriteNode *playAgain;
@property SKSpriteNode *goOn;

@property int passados;

@property CGPoint initLocation;

@property Card *carta1;
@property Card *carta2;
@property Card *carta3;
@property Card *carta4;

@property Slot *slot1;
@property Slot *slot2;
@property Slot *slot3;
@property Slot *slot4;

@property Letra *letra1;
@property Letra *letra2;
@property Letra *letra3;
@property Letra *letra4;

@property NSArray *letters;
@property NSArray *shuffled;

@property bool jaAvancou;

-(void)configure;

@end
