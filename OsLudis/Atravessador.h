//
//  Atravessador.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Semaforo.h"

@class StreetGameScene;

@interface Atravessador : SKSpriteNode

@property Semaforo *mySema;
@property bool atravessando;

-(int)possoAtravesar:(CGPoint)location;

-(int)update;

@end
