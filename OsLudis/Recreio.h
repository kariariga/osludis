//
//  Recreio.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 29/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h> 


@interface Recreio : SKScene <AVAudioPlayerDelegate>

@property bool contando;

@property SKSpriteNode *bg;
@property AVAudioPlayer *bgMus;

@property SKSpriteNode *repeat;
@property SKSpriteNode *next;

@property SKShapeNode *rect;

@end
