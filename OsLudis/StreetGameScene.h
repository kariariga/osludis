//
//  StreetGameScene.h
//  OsLudis
//
//  Created by Ricardo Striffler on 23/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AvFoundation.h>
#import "Semaforo.h"
#import "Atravessador.h"
#import "CarroDir.h"
#import "CarroEsq.h"

@interface StreetGameScene : SKScene

@property bool storyDone;

@property int estado;

@property SKSpriteNode *playAgain;
@property SKSpriteNode *goOn;

@property int life;
@property int contWin;

@property SKSpriteNode *background;

@property Semaforo *semaforo1;
@property Semaforo *semaforo2;
@property Semaforo *semaforo3;

@property Atravessador *atravessador1;
@property Atravessador *atravessador2;
@property Atravessador *atravessador3;

@property CarroDir *carroDir;
@property CarroEsq *carroEsq;

@property SKSpriteNode *life1;
@property SKSpriteNode *life2;
@property SKSpriteNode *life3;

@property SKLabelNode *labelContador;

@property AVAudioPlayer *errou;
@property AVAudioPlayer *theme;

@property bool jaAvancou;

@end
