//
//  main.m
//  OsLudis
//
//  Created by Karina Ariga de Oliveira on 20/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
