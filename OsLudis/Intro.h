//
//  Intro.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Intro : SKScene

@property SKSpriteNode *bg;
@property SKSpriteNode *blu;
@property SKSpriteNode *mao;

@property SKSpriteNode *repeat;

@property AVAudioPlayer *bgMus;

@property bool podeBater;

@end
