//
//  PrincipalScene.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface PrincipalScene : SKScene

@property bool storyDone;

@property SKSpriteNode *bg;

@property SKSpriteNode *bg1;
@property SKSpriteNode *bg2;
@property SKSpriteNode *bg3;

@property SKSpriteNode *play;
@property SKSpriteNode *info;

@property SKSpriteNode *creditos;

@property SKSpriteNode *cards;
@property SKSpriteNode *line;
@property SKSpriteNode *street;
@property SKSpriteNode *hide;

@property SKSpriteNode *lixo;

@property SKSpriteNode *logo;

@property bool up;

@property bool canMove;


-(void)configure;

-(void)salvar;

@end
