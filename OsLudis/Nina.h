//
//  Nina.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@interface Nina : SKScene <AVAudioRecorderDelegate>
{
    AVAudioRecorder *recorder;
}

@property SKSpriteNode *bg;

@property SKSpriteNode *microfone;

@property AVAudioPlayer *bgMus;

@property bool podeAcordar;

@property SKSpriteNode *next;
@property SKSpriteNode *repeat;

@property SKSpriteNode *blu;

@property bool animandoMicrofone;

@property bool permissao;

@end
