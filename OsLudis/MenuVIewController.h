//
//  MenuVIewController.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MenuVIewController : UIViewController

@property AVAudioPlayer *mus;

@property bool terminou;


-(bool)StoryDone;
-(void)saveProgress;

@end
