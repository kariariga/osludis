//
//  Simulacao.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 29/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Simulacao.h"
#import "StreetGameScene.h"

@implementation Simulacao

-(void)didMoveToView:(SKView *)view
{
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"transito"];
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"avanca_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    //BLUNINA
    _bluNina = [SKSpriteNode spriteNodeWithImageNamed:@"bluNina"];
    _bluNina.position = CGPointMake(666,140);
    _bluNina.zPosition = 10;
    [self addChild:_bluNina];
    
    //SEMAFORO
    _semaforo = [SKSpriteNode spriteNodeWithImageNamed:@"red"];
    _semaforo.position = CGPointMake(800, 650);
    _semaforo.zPosition = 30;
    [self addChild:_semaforo];
    
    [self performSelector:@selector(toca:) withObject:@"transito02.mp3" afterDelay:1.0f];
    [self performSelector:@selector(greendeia) withObject:nil afterDelay:8.5f];
    [self performSelector:@selector(deixaAtravesar) withObject:nil afterDelay:6.0f];
    
    _podeAtravessar = false;
    _sinalVerde = false;
    _podeTocar = true;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if(!_jaAtravesou)
        {
            if([_bluNina containsPoint:location])
            {
                if(_podeAtravessar)
                {
                    
                    if(_sinalVerde)
                    {
                        [_bluNina runAction:[SKAction moveToY:1000 duration:3.0f]];
                        _jaAtravesou = true;
                        [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:3.5f];
                    }
                    else
                    {
                        if(_podeTocar)
                        {
                            _podeTocar = false;
                            [self performSelector:@selector(deixaTocar) withObject:nil afterDelay:2.0f];
                            [self runAction:[SKAction playSoundFileNamed:@"transito03.mp3" waitForCompletion:YES]];
                        }
                    }
                }
            }
        }
        
        if([_repeat containsPoint:location])
        {
            [_bgMus stop];
            Simulacao *scene = [Simulacao sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.3f]];
        }
        if([_next containsPoint:location])
        {
            [_bgMus stop];
            
            StreetGameScene *scene = [StreetGameScene sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
        }
    }
}

-(void)deixaTocar
{
    _podeTocar = true;
}

-(void)deixaAtravesar
{
    _podeAtravessar = true;
}

-(void)atravesou
{
    [self mostraBotoes];
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)greendeia
{
    _sinalVerde = true;
    _semaforo.texture = [SKTexture textureWithImageNamed:@"green"];
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

@end
