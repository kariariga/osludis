//
//  CarroDir.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Semaforo.h"

@interface CarroDir : SKSpriteNode


@property int ponto;
@property int nextX;

@property Semaforo *sema1;
@property Semaforo *sema2;
@property Semaforo *sema3;

-(void)gotoNextPoint;

-(void)update;

@end
