//
//  Letra.h
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Letra : SKSpriteNode

@property CGPoint destinationPoint;

-(void)goTodestination;

@end
