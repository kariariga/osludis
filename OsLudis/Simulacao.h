//
//  Simulacao.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 29/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Simulacao : SKScene <AVAudioPlayerDelegate>

@property AVAudioPlayer *bgMus;

@property SKSpriteNode *repeat;
@property SKSpriteNode *next;

@property SKSpriteNode *bluNina;
@property SKSpriteNode *semaforo;

@property bool sinalVerde;
@property bool podeAtravessar;
@property bool jaAtravesou;

@property bool podeTocar;
@end
