//
//  CarroDir.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "CarroDir.h"

@implementation CarroDir

-(void)gotoNextPoint
{
    
}

-(void)update
{
    switch(_ponto)
    {
        case 1:
            if(self.position.x == _nextX)
            {
                if(!_sema1.aberto)
                {
                    if(!_sema1.atravessando)
                    {
                        _ponto = 2;
                        _nextX = 431;
                        [self runAction:[SKAction moveToX:_nextX duration:1.0f]];
                    }
                    
                }
            }
            break;
        case 2:
            if(self.position.x == _nextX)
            {
                if(!_sema2.aberto)
                {
                    if(!_sema2.atravessando)
                    {
                        _ponto = 3;
                        _nextX = 923;
                        [self runAction:[SKAction moveToX:_nextX duration:1.0f]];
                    }
                }
            }
            break;
        case 3:
            if(self.position.x == _nextX)
            {
                if(!_sema3.aberto)
                {
                    if(!_sema3.atravessando)
                    {
                        _ponto = 4;
                        _nextX = 1500;
                        [self runAction:[SKAction moveToX:_nextX duration:1.0f]];
                    }
                }
            }
            break;
        case 4:
            if(self.position.x == _nextX)
            {
                int r = arc4random()%2;
                if(r == 0)
                {
                    self.texture = [SKTexture textureWithImageNamed:@"carro1"];
                }
                else
                {
                    self.texture = [SKTexture textureWithImageNamed:@"carro4"];
                }
                
                _nextX = 0;
                _ponto = 1;
                self.position = CGPointMake(-300, 233);
                [self runAction:[SKAction moveToX:_nextX duration:0.5]];
            }
            break;
    }
}

@end
