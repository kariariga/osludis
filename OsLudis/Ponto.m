//
//  Ponto.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//
#import "Ponto.h"

@implementation Ponto

-(void)conect:(Ponto *)ponto
{
    if(ponto == _next && !_conectouNext)
    {
        /*
        SKShapeNode *yourline = [SKShapeNode node];
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
        CGPathMoveToPoint(pathToDraw, NULL, self.position.x, self.position.y+20);
        CGPathAddLineToPoint(pathToDraw, NULL, ponto.position.x, ponto.position.y+20);
        yourline.path = pathToDraw;
        [yourline setStrokeColor:[UIColor blueColor]];
        [self.parent addChild:yourline];
        */
        
        SKSpriteNode *rope = [SKSpriteNode spriteNodeWithImageNamed:@"corda"];
        rope.zPosition = -100;
        float distance = hypotf(ponto.position.x - self.position.x, ponto.position.y - self.position.y);
        rope.size = CGSizeMake(distance, 100);
        
        rope.position = CGPointMake(self.position.x +(ponto.position.x - self.position.x)/2,self.position.y + (ponto.position.y - self.position.y)/2);
        
        CGFloat angle = atan2f(ponto.position.y - self.position.y, ponto.position.x - self.position.x);
        
        if (rope.zRotation < 0) {
            rope.zRotation = rope.zRotation + M_PI * 2;
        }
        
        [rope runAction:[SKAction rotateToAngle:angle duration:0]];
        
        [self.parent addChild:rope];
        
        [self runAction:[SKAction playSoundFileNamed:@"corda.wav" waitForCompletion:NO]];
        _conectouNext = true;
        ponto.conectouback = true;
    }
    if(ponto == _back && !_conectouback)
    {
        /*
        SKShapeNode *yourline = [SKShapeNode node];
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
        CGPathMoveToPoint(pathToDraw, NULL, self.position.x, self.position.y+20);
        CGPathAddLineToPoint(pathToDraw, NULL, ponto.position.x, ponto.position.y+20);
        yourline.path = pathToDraw;
        [yourline setStrokeColor:[UIColor blueColor]];
        [self.parent addChild:yourline];
        */
        
        SKSpriteNode *rope = [SKSpriteNode spriteNodeWithImageNamed:@"corda"];
        rope.zPosition = -100;
        float distance = hypotf(ponto.position.x - self.position.x, ponto.position.y - self.position.y);
        rope.size = CGSizeMake(distance, 100);
        
        rope.position = CGPointMake(self.position.x +(ponto.position.x - self.position.x)/2,self.position.y + (ponto.position.y - self.position.y)/2);
        
        CGFloat angle = atan2f(ponto.position.y - self.position.y, ponto.position.x - self.position.x);
        
        if (rope.zRotation < 0) {
            rope.zRotation = rope.zRotation + M_PI * 2;
        }
        
        [rope runAction:[SKAction rotateToAngle:angle duration:0]];
        
        [self.parent addChild:rope];

        [self runAction:[SKAction playSoundFileNamed:@"corda.wav" waitForCompletion:NO]];
        ponto.conectouNext = true;
        _conectouback = true;
    }
}

-(bool)jaConectado
{
    return (_conectouback && _conectouNext);
}

@end
