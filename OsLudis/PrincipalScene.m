//
//  PrincipalScene.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "PrincipalScene.h"
#import "InfoScene.h"

#import "Intro.h"

#import "GameScene.h"
#import "LineGameScene.h"
#import "StreetGameScene.h"
#import "HideGameScene.h"

#import "MenuVIewController.h"


@implementation PrincipalScene

-(void)didMoveToView:(SKView *)view
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"playMusic" object:nil userInfo:nil];
    
    _storyDone = [self StoryDone];
    
    _up = false;
    _canMove = true;
    
    _bg = [SKSpriteNode spriteNodeWithImageNamed:@"Background1"];
    _bg.position = CGPointMake(666, 375);
    _bg.zPosition = -100;
    [self addChild:_bg];
    
    _bg1 = [SKSpriteNode spriteNodeWithImageNamed:@"Background_stars_1"];
    _bg1.position = CGPointMake(666, 375);
    _bg1.zPosition = -100;
    _bg1.alpha = 0;
    [self addChild:_bg1];
    
    _bg2 = [SKSpriteNode spriteNodeWithImageNamed:@"Background_stars_2"];
    _bg2.position = CGPointMake(666, 375);
    _bg2.zPosition = -100;
    _bg2.alpha = 0;
    [self addChild:_bg2];
    
    _bg3 = [SKSpriteNode spriteNodeWithImageNamed:@"Background_stars_3"];
    _bg3.position = CGPointMake(666, 375);
    _bg3.zPosition = -100;
    _bg3.alpha = 0;
    [self addChild:_bg3];
    
    int r = arc4random()%4;
    [self performSelector:@selector(clareiaBg:) withObject:_bg1 afterDelay:r];
    
    r = arc4random()%4;
    [self performSelector:@selector(clareiaBg:) withObject:_bg2 afterDelay:r];
    
    r = arc4random()%4;
    [self performSelector:@selector(clareiaBg:) withObject:_bg3 afterDelay:r];
    
    
    _logo = [SKSpriteNode spriteNodeWithImageNamed:@"logo"];
    _logo.position = CGPointMake(666, 550);
    _logo.xScale = .5;
    _logo.yScale = .5;
    _logo.zPosition = 30;
    [self addChild:_logo];
    
    _play = [SKSpriteNode spriteNodeWithImageNamed:@"play_batata"];
    _play.position = CGPointMake(666, 230);
    _play.xScale = .5;
    _play.yScale = .5;
    [self cresceBotaoPlay];
    [self addChild:_play];
    
    _info = [SKSpriteNode spriteNodeWithImageNamed:@"info_batata"];
    _info.position = CGPointMake(1250, 650);
    _info.xScale = .6;
    _info.yScale = .6;
    [self addChild:_info];
    
    _cards = [SKSpriteNode spriteNodeWithImageNamed:@"ico_cards"];
    _cards.position = CGPointMake(450, 300);
    _cards.xScale = .8;
    _cards.yScale = .8;
    [self addChild:_cards];
    
    
    _street = [SKSpriteNode spriteNodeWithImageNamed:@"ico_street"];
    _street.position = CGPointMake(250, 400);
    _street.xScale = .8;
    _street.yScale = .8;
    [self addChild:_street];
    
    _line = [SKSpriteNode spriteNodeWithImageNamed:@"ico_line"];
    _line.position = CGPointMake(1082, 400);
    _line.xScale = .8;
    _line.yScale = .9;
    [self addChild:_line];
    
    _hide  = [SKSpriteNode spriteNodeWithImageNamed:@"ico_hide"];
    _hide.position = CGPointMake(882, 300);
    _hide.xScale = .8;
    _hide.yScale = .8;
    [self addChild:_hide];
    
    _creditos = [SKSpriteNode spriteNodeWithImageNamed:@"creditos"];
    _creditos.position = CGPointMake(666, 375 - 750);
    [self addChild:_creditos];
    
    _lixo = [SKSpriteNode spriteNodeWithImageNamed:@"lixo"];
    _lixo.position = CGPointMake(100, 100);
    [self addChild:_lixo];
    
    [self configure];
}

-(void)configure
{
    if([self storyDone]==false)
    {
        CGPoint pon = CGPointMake(-300, -300);
        _hide.position = pon;
        _line.position = pon;
        _cards.position = pon;
        _street.position = pon;
    }
    else
    {
        _cards.position = CGPointMake(450, 300);
        _street.position = CGPointMake(250, 400);
        _line.position = CGPointMake(1082, 400);
        _hide.position = CGPointMake(882, 300);
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if(_up)
        {
            if(_canMove)
            {
                [self moveAllDown];
                _up = false;
            }
        }
        else
        {
            if([_play containsPoint:location])
            {
                Intro *scene = [Intro sceneWithSize:CGSizeMake(1335, 750)];
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"vaiPraHistoria" object:nil userInfo:nil];
            }
            
            if([_info containsPoint:location])
            {
                if(_canMove)
                {
                    _up = true;
                    [self moveAllUp];
                }
            }
            
            if([_cards containsPoint:location])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMusic" object:nil userInfo:nil];
                GameScene *scene = [GameScene sceneWithSize:CGSizeMake(1335, 750)];
                NSArray *letters = @[ @"F", @"O", @"C",@"A"];
                NSArray *shuffled= @[ @3, @2, @1, @0];
                scene.letters = letters;
                scene.shuffled = shuffled;
                scene.passados = 0;
                scene.storyDone = true;
                [scene configure];
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4F]];
            }
            
            if([_street containsPoint:location])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMusic" object:nil userInfo:nil];
                StreetGameScene *scene = [StreetGameScene sceneWithSize:CGSizeMake(1335, 750)];
                scene.storyDone = true;
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:1.0F]];
            }
            
            if([_line containsPoint:location])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMusic" object:nil userInfo:nil];
                LineGameScene *scene = [LineGameScene sceneWithSize:CGSizeMake(1335, 750)];
                scene.fase = 1;
                scene.storyDone = true;
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:1.0F]];
            }
            
            if([_hide containsPoint:location])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMusic" object:nil userInfo:nil];
                HideGameScene *scene = [HideGameScene sceneWithSize:CGSizeMake(1335, 750)];
                scene.esperando = false;
                scene.storyDone = true;
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:1.0F]];
            }
            
            if([_lixo containsPoint:location])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"erase" object:nil userInfo:nil];
                [self eraseProgress];
                PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene];
            }
        }
    }
}




-(void)cresceBotaoPlay
{
    [_play runAction:[SKAction scaleTo:.6 duration:1.0f]];
    [self performSelector:@selector(descreceBotaoPlay) withObject:nil afterDelay:1.0f];
}

-(void)descreceBotaoPlay
{
    [_play runAction:[SKAction scaleTo:.5 duration:1.0f]];
    [self performSelector:@selector(cresceBotaoPlay) withObject:nil afterDelay:1.0f];
}

-(void)clareiaBg:(SKSpriteNode *)bg
{
    int r = arc4random()%4;
    [bg runAction:[SKAction fadeAlphaTo:1 duration:1.0f]];
    [self performSelector:@selector(escureBg:) withObject:bg afterDelay:r];
}

-(void)escureBg:(SKSpriteNode *)bg
{
    int r = arc4random()%4;
    [bg runAction:[SKAction fadeAlphaTo:0 duration:1.0f]];
    [self performSelector:@selector(clareiaBg:) withObject:bg afterDelay:r];
}

-(void)rotaciona_15:(SKSpriteNode *)ico
{
    [ico runAction:[SKAction rotateToAngle:30 duration:1.0f shortestUnitArc:YES]];
    [self performSelector:@selector(plusRotaciona_15:) withObject:ico afterDelay:1.0f];
}

-(void)plusRotaciona_15:(SKSpriteNode *)ico
{
    [ico runAction:[SKAction rotateToAngle:15 duration:1.0f shortestUnitArc:YES]];
    [self performSelector:@selector(rotaciona_15:) withObject:ico afterDelay:1.0f];
}

-(void)setCanMove
{
    _canMove = true;
}

-(void)moveAllUp
{
    [_play runAction:[SKAction moveToY:_play.position.y + 750 duration:0.5f]];
    [_info runAction:[SKAction moveToY:_info.position.y + 750 duration:0.5f]];
    [_creditos runAction:[SKAction moveToY:_creditos.position.y + 750 duration:0.5f]];
    [_cards runAction:[SKAction moveToY:_cards.position.y + 750 duration:0.5f]];
    [_hide runAction:[SKAction moveToY:_hide.position.y + 750 duration:0.5f]];
    [_line runAction:[SKAction moveToY:_line.position.y + 750 duration:0.5f]];
    [_street runAction:[SKAction moveToY:_street.position.y + 750 duration:0.5f]];
    [_lixo runAction:[SKAction moveToY:_lixo.position.y + 750 duration:0.5f]];
    [_logo runAction:[SKAction moveToY:_logo.position.y + 750 duration:0.5f]];
    _canMove = false;
    [self performSelector:@selector(setCanMove) withObject:nil afterDelay:0.5f];
}

-(void)moveAllDown
{
    [_play runAction:[SKAction moveToY:_play.position.y - 750 duration:0.5f]];
    [_info runAction:[SKAction moveToY:_info.position.y - 750 duration:0.5f]];
    [_creditos runAction:[SKAction moveToY:_creditos.position.y - 750 duration:0.5f]];
    [_cards runAction:[SKAction moveToY:_cards.position.y - 750 duration:0.5f]];
    [_hide runAction:[SKAction moveToY:_hide.position.y - 750 duration:0.5f]];
    [_line runAction:[SKAction moveToY:_line.position.y - 750 duration:0.5f]];
    [_street runAction:[SKAction moveToY:_street.position.y - 750 duration:0.5f]];
    [_lixo runAction:[SKAction moveToY:_lixo.position.y - 750 duration:0.5f]];
    [_logo runAction:[SKAction moveToY:_logo.position.y - 750 duration:0.5f]];
    
    _canMove = false;
    [self performSelector:@selector(setCanMove) withObject:nil afterDelay:0.5f];
}

-(void)salvar
{
    [self saveProgress];
}

#pragma mark SAVE AND LOAD

-(void)saveProgress
{
    //NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    
    
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    
    NSLog(@"Estou salvando do arquivo: %@", fileAtPath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath])
    {
        NSString *text = @"This file exist just to assure that the user has finished the Story Mode.";
        [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    else
    {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        NSString *text = @"This file exist just to assure that the user has finished the Story Mode.";
        [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
}

-(bool)StoryDone;
{
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    //NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    NSLog(@"Estou carreagando do arquivo: %@", fileAtPath);
    
    return([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]);
}

-(void)eraseProgress;
{
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    //NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    [[NSFileManager defaultManager]removeItemAtPath:fileAtPath error:nil];
}

@end
