//
//  Intro.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Intro.h"
#import "Nina.h"

@implementation Intro

-(void)didMoveToView:(SKView *)view
{
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];
    
    //BACKGROUND
    _bg = [SKSpriteNode spriteNodeWithImageNamed:@"introBG"];
    _bg.position = CGPointMake(666, 375);
    _bg.zPosition = -10;
    [self addChild:_bg];
    
    //BLU
    _blu = [SKSpriteNode spriteNodeWithImageNamed:@"Blu"];
    _blu.position = CGPointMake(650, 375);
    _blu.xScale = .8;
    _blu.yScale = .8;
    _blu.zPosition = -5;
    [self addChild:_blu];
    
    //MAO
    _mao = [SKSpriteNode spriteNodeWithImageNamed:@"maoblu"];
    _mao.position = CGPointMake(455, 450);
    _mao.zPosition = 0;
    [self addChild:_mao];
    
    
    //FILTRO PRETO
    SKShapeNode *rect = [SKShapeNode shapeNodeWithRect:CGRectMake(-1, -1, 1337, 752)];
    rect.zPosition = 100;
    rect.fillColor = [SKColor blackColor];
    [self addChild:rect];
    
    [self performSelector:@selector(toca:) withObject:@"intro01.mp3" afterDelay:0.5f];
    [self performSelector:@selector(toca:) withObject:@"intro01a.mp3" afterDelay:3.5f];
    [self performSelector:@selector(toca:) withObject:@"intro01b.mp3" afterDelay:9.0f];
    [self performSelector:@selector(toca:) withObject:@"intro01c.mp3" afterDelay:15.0f];
    [self performSelector:@selector(blackground:) withObject:rect afterDelay:16.0f];
    [self performSelector:@selector(toca:) withObject:@"intro02.mp3" afterDelay:20.0f];
    [self performSelector:@selector(toca:) withObject:@"intro03.mp3" afterDelay:22.0f];
    [self performSelector:@selector(deixaBater) withObject:nil afterDelay:24.5f];
    
    _podeBater = false;
    
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if(_podeBater)
        {
            if([_mao containsPoint:location])
            {
                [_bgMus stop];
                Nina *scene = [Nina sceneWithSize:CGSizeMake(1335, 750)];
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene];
            }
            
            if([_repeat containsPoint:location])
            {
                Intro *scene = [Intro sceneWithSize:CGSizeMake(1335, 750)];
                scene.scaleMode = SKSceneScaleModeAspectFit;
                [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.4f]];
            }
        }
    }
}

-(void)deixaBater
{
    _podeBater = true;
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)blackground:(SKSpriteNode *)bg
{
    [bg runAction:[SKAction fadeAlphaTo:0 duration:1.0f]];
}

@end
