//
//  Fim.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Fim.h"
#import "PrincipalScene.h"

@implementation Fim

-(void)didMoveToView:(SKView *)view
{
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"final"];
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"home_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    [self performSelector:@selector(toca:) withObject:@"final01.mp3" afterDelay:1.0f];
    [self performSelector:@selector(toca:) withObject:@"final02.mp3" afterDelay:6.0f];
    [self performSelector:@selector(toca:) withObject:@"final03.mp3" afterDelay:7.0f];
    [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:9.0f];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if([_repeat containsPoint:location])
        {
            [_bgMus stop];
            Fim *scene = [Fim sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.3f]];
        }
        if([_next containsPoint:location])
        {
            [_bgMus stop];
            
            if(![self StoryDone])
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PARABÉNS!!!" message:@"Você terminou o modo historia. Liberou todos os mini-games no menu." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *acionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
                
                [alert addAction:acionOk];
                
                [self.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
            }
            
            
            PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
            [scene salvar];
            [scene configure];
            [scene reloadInputViews];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:1.0F]];
        }
    }
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

-(bool)StoryDone;
{
    //NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    return([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]);
}


@end
