//
//  MenuVIewController.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "MenuVIewController.h"
#import "PrincipalScene.h"
#import "InfoScene.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation MenuVIewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _terminou == [self StoryDone];
    
    NSError *erro;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&erro];
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    
    
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _mus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _mus.numberOfLoops = -1;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vaiPraHistoria:) name:@"vaiPraHistoria" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(desejaApagar) name:@"erase" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playMusic) name:@"playMusic" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopMusic) name:@"stopMusic" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveProgress) name:@"salvar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDone) name:@"carregar" object:nil];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
    CGSize size;
    size.width = 1334;
    size.height = 750;
    
    PrincipalScene *scene = [PrincipalScene sceneWithSize:size];
    
    scene.storyDone = [self StoryDone];
    
    scene.scaleMode = SKSceneScaleModeAspectFit;
    
    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)vaiPraHistoria:(NSNotification *)notification
{
    [_mus stop];
}

-(void)desejaApagar
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"ATENÇÃO" message:@"O progresso foi apagado." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        return;
    }];
    
    [alert addAction:actionOk];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)playMusic
{
    [_mus play];
}

-(void)stopMusic
{
    [_mus stop];
}

-(void)saveProgress
{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath])
    {
        NSString *text = @"This file exist just to assure that the user has finished the Story Mode.";
        [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    else
    {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        NSString *text = @"This file exist just to assure that the user has finished the Story Mode.";
        [[text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
}

-(bool)StoryDone;
{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    return([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]);
}

-(void)eraseProgress;
{
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Profile.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    [[NSFileManager defaultManager]removeItemAtPath:fileAtPath error:nil];
}

@end
