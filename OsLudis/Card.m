//
//  carta.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "Card.h"

@implementation Card

-(void)onDrag:(CGPoint)location
{
    if(!_locked)
    {
        [self runAction:[SKAction playSoundFileNamed:@"card_sound.mp3" waitForCompletion:NO]];
        _offset = CGPointMake(self.position.x - location.x,self.position.y - location.y);
        _dragging = true;
        self.xScale *= 1.2;
        self.yScale *= 1.2;
        
        self.zPosition = 10;
    }
}

-(void)updatePosition:(CGPoint)location;
{
    if(_dragging)
    {
        self.position = CGPointMake(location.x + _offset.x, location.y + _offset.y);

    }
}

-(void)onDrop:(Slot *)slot
{
    if(_dragging)
    {
        self.xScale /= 1.2;
        self.yScale /= 1.2;
        self.zPosition = 0;
        _dragging = false;
        if(slot != nil)
        {
            if([slot.value isEqualToString:self.value])
            {
                CGFloat xDist = (slot.position.x - self.position.x);
                CGFloat yDist = (slot.position.y - self.position.y);
                CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
                
                if(distance < 80)
                {
                    self.position = slot.position;
                    _locked = true;
                    [self runAction:[SKAction playSoundFileNamed:@"card_sound.mp3" waitForCompletion:NO]];
                    self.zPosition = -1;
                }
                else
                {
                    SKAction *action = [SKAction moveTo:_initialPosition duration:0.5f];
                    [self runAction:action];
                }
            }
            else
            {
                SKAction *action = [SKAction moveTo:_initialPosition duration:0.5f];
                [self runAction:action];
            }
        }
        else
        {
            SKAction *action = [SKAction moveTo:_initialPosition duration:0.5f];
            [self runAction:action];
        }
    }
}

@end
