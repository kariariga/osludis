//
//  Recreio.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 29/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Recreio.h"
#import "HideGameScene.h"

@implementation Recreio

-(void)didMoveToView:(SKView *)view
{
    
    _contando = false;
    
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];

    
    //BACKGROUND
    _bg = [SKSpriteNode spriteNodeWithImageNamed:@"recreio"];
    _bg.position = CGPointMake(666, 375);
    [self addChild:_bg];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"avanca_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    
    [self performSelector:@selector(toca:) withObject:@"esconde01.mp3" afterDelay:0.5f];
    [self performSelector:@selector(changeImage:) withObject:@"recreio00" afterDelay:4.5f];
    [self performSelector:@selector(toca:) withObject:@"esconde02.mp3" afterDelay:5.0f];
    [self performSelector:@selector(toca:) withObject:@"esconde03.mp3" afterDelay:7.5f];
    [self performSelector:@selector(blackground) withObject:nil afterDelay:10.5f];
    _contando = true;
    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:0] afterDelay:10.5f];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if([_repeat containsPoint:location])
        {
            [_bgMus stop];
            Recreio *scene = [Recreio sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.3f]];
        }
        if([_next containsPoint:location])
        {
            [_bgMus stop];
            
            HideGameScene *scene = [HideGameScene sceneWithSize:CGSizeMake(1335, 750)];
            scene.esperando = false;
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
        }
    }

}

-(void)update:(NSTimeInterval)currentTime
{
    
}


-(void)contar:(NSNumber *)num
{
    UIDeviceOrientation ori = [[UIDevice currentDevice]orientation];
    
    if(_contando)
    {
        int atual = [num intValue];
        
        if(ori == UIDeviceOrientationFaceDown)
            
        {
            switch([num intValue])
            {
                case 0:
                    [self toca:@"esconde04.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:1.8f];
                    break;
                case 1:
                    [self toca:@"1.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 2:
                    [self toca:@"2.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 3:
                    [self toca:@"3.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 4:
                    [self toca:@"4.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 5:
                    [self toca:@"5.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 6:
                    [self toca:@"6.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 7:
                    [self toca:@"7.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 8:
                    [self toca:@"8.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 9:
                    [self toca:@"9.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.8f];
                    break;
                case 10:
                    [self toca:@"10.mp3"];
                    atual++;
                    [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:0.3f];
                    break;
                case 11:
                    _contando = false;
                    [self jacontou];
            }
            
        }
        else
        {
            if(atual == 0)
            {
                [self toca:@"esconde03.mp3"];
                [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:4.0f];
            }
            else
            {
                [self toca:@"esconde06.mp3"];
                [self performSelector:@selector(contar:) withObject:[NSNumber numberWithInt:atual] afterDelay:4.0f];
            }
        }
        
    }
}

-(void)jacontou
{
    [self performSelector:@selector(toca:) withObject:@"esconde07.mp3" afterDelay:0.0];
    [self performSelector:@selector(toca:) withObject:@"esconde08.mp3" afterDelay:1.5f];
    [self performSelector:@selector(changeImage:) withObject:@"recreio01" afterDelay:1.5f];
    [self performSelector:@selector(clareiaBg) withObject:nil afterDelay:1.5f];
    [self performSelector:@selector(toca:) withObject:@"esconde08a.mp3" afterDelay:3.0f];
    [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:5.0f];
}

-(void)blackground
{
    _rect = [SKShapeNode shapeNodeWithRect:CGRectMake(-1, -1, 1337, 753)];
    _rect.fillColor = [SKColor blackColor];
    _rect.zPosition = 100;
    [self addChild:_rect];
}

-(void)clareiaBg
{
    [_rect runAction:[SKAction fadeAlphaTo:0 duration:0.4f]];
}
     
-(void)changeImage:(NSString *)image
{
    _bg.texture = [SKTexture textureWithImageNamed:image];
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

@end
