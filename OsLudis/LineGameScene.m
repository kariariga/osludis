//
//  LineGameScene.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "LineGameScene.h"
#import "Fim.h"
#import "PrincipalScene.h"

@implementation LineGameScene

-(void)didMoveToView:(SKView *)view
{
    
    
    _jaAvancou = false;
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"theme"
                                         ofType:@"mp3"]];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.numberOfLoops = -1;
    
    [player play];
    
    
    /* begin */
    _mao = [SKSpriteNode spriteNodeWithImageNamed:@"mao"];
    _mao.zPosition = 100;
    _mao.xScale = .4;
    _mao.yScale = .4;
    _mao.position = CGPointMake(-100, -100);
    [self addChild:_mao];
    
    _estado = 1;
    _playAgain = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    if([self storyDone])
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"home_batata"];
    else
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"play_batata"];
    
    _playAgain.position = CGPointMake(400, 900);
    _playAgain.zPosition = 400;
    _goOn.position = CGPointMake(934, 900);
    _goOn.zPosition = 400;
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"madeira"];
    bg.zPosition = -3000;
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
    
    _drawning = false;
    
    [self configure];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _drawning = false;
    
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if(_estado == 1)
        {
            Ponto *alvo = nil;
            
            switch(_fase)
            {
                case 1:
                    alvo = [self touchPipa:location];
                    break;
                case 2:
                    alvo = [self touchCasa:location];
                    break;
                case 3:
                    alvo = [self touchEstrela:location];
                    break;
                case 4:
                    alvo = [self touchPeixe:location];
                    break;
                case 5:
                    alvo = [self touchGato:location];
                    break;
            }
            
            if(alvo != nil)
            {
                _origin = alvo;
                _lineDown = alvo.position;
                _drawning = true;
            }
        }
        if(_estado == 2)
        {
            if([_playAgain containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    CGSize size;
                    size.width = 1334;
                    size.height = 750;
                    LineGameScene *scene = [LineGameScene sceneWithSize:size];
                    scene.fase = 1;
                    scene.storyDone = [self storyDone];
                    scene.scaleMode = SKSceneScaleModeAspectFit;
                    [self.view presentScene:scene transition:[SKTransition doorsCloseHorizontalWithDuration:1.0f]];
                    _jaAvancou  =true;
                }
            }
            if([_goOn containsPoint:location])
            {
                
                if(_jaAvancou == false)
                {
                    [player stop];
                    if([self storyDone] == false)
                    {
                        [self removeAllChildren];
                        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"pontos01.png"];
                        [self runAction:[SKAction playSoundFileNamed:@"sirene.wav" waitForCompletion:NO]];
                        bg.position = CGPointMake(666, 375);
                        [self addChild:bg];
                        _estado = 40;
                        [self performSelector:@selector(enviaMensagemAcabouLine) withObject:nil afterDelay:2.0f];
                    }
                    else
                    {
                        [self enviaMensagemAcabouLine];
                    }
                    _jaAvancou = true;
                }
            }
        }
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        _mao.position = CGPointMake(location.x + 30, location.y - 30);
    }
    
    [_yourLine removeFromParent];
    if(_drawning)
    {
    
        for(UITouch *touch in touches)
        {
            CGPoint location = [touch locationInNode:self];
            
            _lineMove = location;
        
            _yourLine = [SKShapeNode node];
            CGMutablePathRef pathToDraw = CGPathCreateMutable();
            CGPathMoveToPoint(pathToDraw, NULL, _lineDown.x, _lineDown.y+20);
            CGPathAddLineToPoint(pathToDraw, NULL, _lineMove.x, _lineMove.  y);
            _yourLine.path = pathToDraw;
            [_yourLine setStrokeColor:[UIColor redColor]];
            [self addChild:_yourLine];
            
            
            Ponto *alvo = nil;
            
            switch(_fase)
            {
                case 1:
                    alvo = [self touchPipa:location];
                    break;
                case 2:
                    alvo = [self touchCasa:location];
                    break;
                case 3:
                    alvo = [self touchEstrela:location];
                    break;
                case 4:
                    alvo = [self touchPeixe:location];
                    break;
                case 5:
                    alvo = [self touchGato:location];
                    break;
            }
            
            if(alvo != nil)
            {
                [_origin conect:alvo];
                _origin = alvo;
                _lineDown = _origin.position;
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    _mao.position = CGPointMake(-100, -100);
    
    [_yourLine removeFromParent];
    if(_drawning)
    {
        for(UITouch *touch in touches)
        {
            CGPoint location = [touch locationInNode:self];
            
            Ponto *alvo;
            
            if(_estado == 1)
            {
                switch(_fase)
                {
                    case 1:
                        alvo = [self touchPipa:location];
                        break;
                    case 2:
                        alvo = [self touchCasa:location];
                        break;
                    case 3:
                        alvo = [self touchEstrela:location];
                        break;
                    case 4:
                        alvo = [self touchPeixe:location];
                        break;
                    case 5:
                        alvo = [self touchGato:location];
                        break;
                }
                
                if(alvo != nil)
                {
                    [_origin conect:alvo];
                }
            }
        }
    }
    
    if(_estado == 1)
    {
        switch(_fase)
        {
            case 1:
                [self checkPipa];
                break;
            case 2:
                [self checkCasa];
                break;
            case 3:
                [self checkEstrela];
                break;
            case 4:
                [self checkPeixe];
                break;
            case 5:
                [self checkGato];
                break;
        }
    }
    
    CGPoint null = CGPointMake(0, 0);
    _lineMove = null;
    _lineDown = null;
}

-(void)removeSelf
{
    if(_fase <5)
    {
        CGSize size;
        size.width = 1334;
        size.height = 750;
        LineGameScene *scene = [LineGameScene sceneWithSize:size];
        scene.fase = _fase+1;
        scene.storyDone = [self storyDone];
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [player stop];
        [self.view presentScene:scene transition:[SKTransition pushWithDirection:SKTransitionDirectionDown duration:1.0f]];
    }
    else
    {
        _estado = 2;
        [self addChild:_playAgain];
        [self addChild:_goOn];
        SKShapeNode *rec = [SKShapeNode shapeNodeWithRect:CGRectMake(-5, -5, 1500, 800)];
        rec.alpha = 0;
        rec.zPosition = 300;
        rec.fillColor = [SKColor blackColor];
        [rec runAction:[SKAction fadeAlphaTo:.8 duration:0.5f]];
        [self addChild:rec];
        
        [_playAgain runAction:[SKAction moveToY:375 duration:1.0f]];
        [_goOn runAction:[SKAction moveToY:375 duration:1.0f]];
    }
}

-(void)drawResult
{
    SKSpriteNode *desenho;
    switch(_fase)
    {
        case 1:
            desenho = [SKSpriteNode spriteNodeWithImageNamed:@"pipa"];
            desenho.position = CGPointMake(666, 300);
            break;
        case 2:
            desenho = [SKSpriteNode spriteNodeWithImageNamed:@"casa"];
            desenho.position = CGPointMake(666, 400);
            break;
        case 3:
            desenho = [SKSpriteNode spriteNodeWithImageNamed:@"estrela"];
            desenho.position = CGPointMake(666, 400);
            break;
        case 4:
            desenho = [SKSpriteNode spriteNodeWithImageNamed:@"peixe"];
            desenho.position = CGPointMake(666, 400);
            break;
        case 5:
            desenho = [SKSpriteNode spriteNodeWithImageNamed:@"gato"];
            desenho.position = CGPointMake(666, 350);
            break;
    }
    
    desenho.alpha = 0;
    [desenho runAction:[SKAction fadeAlphaTo:1 duration:1.0f]];
    [self addChild:desenho];
}

-(void)update:(NSTimeInterval)currentTime
{
    
}



-(void)configure
{
    switch(_fase)
    {
        case 1:
            [self setupPipa];
            break;
        case 2:
            [self setupCasa];
            break;
        case 3:
            [self setupEstrela];
            break;
        case 4:
            [self setupPeixe];
            break;
        case 5:
            [self setupGato];
            break;
    }
}

-(void)clearScene
{
    [self removeAllChildren];
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"madeira"];
    bg.zPosition = -3000;
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
}


-(Ponto *)setupPonto:(Ponto *)ponto withPosition:(CGPoint)position
{
    ponto = [Ponto spriteNodeWithImageNamed:@"ponto"];
    ponto.position = position;
    ponto.conectouback = false;
    ponto.conectouNext = false;
    ponto.xScale = .4;
    ponto.yScale = .4;
    [self addChild:ponto];
    return ponto;
}

-(Ponto *)setNext:(Ponto *)next andBack:(Ponto *)back forPonto:(Ponto *)ponto
{
    ponto.next = next;
    ponto.back = back;
    return ponto;
}

#pragma mark PIPA

-(void)setupPipa
{
    _ponto1 = [self setupPonto:_ponto1 withPosition:CGPointMake(620, 165)];
    _ponto2 = [self setupPonto:_ponto1 withPosition:CGPointMake(920,320)];
    _ponto3 = [self setupPonto:_ponto1 withPosition:CGPointMake(900,600)];
    _ponto4 = [self setupPonto:_ponto1 withPosition:CGPointMake(630, 510)];
    
    _ponto1 = [self setNext:_ponto2 andBack:_ponto4 forPonto:_ponto1];
    _ponto2 = [self setNext:_ponto3 andBack:_ponto1 forPonto:_ponto2];
    _ponto3 = [self setNext:_ponto4 andBack:_ponto2 forPonto:_ponto3];
    _ponto4 = [self setNext:_ponto1 andBack:_ponto3 forPonto:_ponto4];
}

-(void)checkPipa
{
    if( [_ponto1 jaConectado] && [_ponto2 jaConectado] && [_ponto3 jaConectado] && [_ponto4 jaConectado])
    {
        [self clearScene];
        [self drawResult];
        [_ponto1 removeFromParent];
        [_ponto2 removeFromParent];
        [_ponto3 removeFromParent];
        [_ponto4 removeFromParent];
        _estado = 2;
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:1.0f];
    }
}

-(Ponto *)touchPipa:(CGPoint)location
{
    Ponto *alvo = nil;
    
    if([_ponto1 containsPoint:location])
        alvo = _ponto1;
    if([_ponto2 containsPoint:location])
        alvo = _ponto2;
    if([_ponto3 containsPoint:location])
        alvo = _ponto3;
    if([_ponto4 containsPoint:location])
        alvo = _ponto4;
    
    return alvo;
}

#pragma mark CASA

-(void)setupCasa
{
    _ponto1 = [self setupPonto:_ponto1 withPosition:CGPointMake(410, 60)];
    _ponto2 = [self setupPonto:_ponto1 withPosition:CGPointMake(965,60)];
    _ponto3 = [self setupPonto:_ponto1 withPosition:CGPointMake(965,480)];
    _ponto4 = [self setupPonto:_ponto1 withPosition:CGPointMake(685, 705)];
    _ponto5 = [self setupPonto:_ponto1 withPosition:CGPointMake(400, 480)];
    
    _ponto1 = [self setNext:_ponto2 andBack:_ponto5 forPonto:_ponto1];
    _ponto2 = [self setNext:_ponto3 andBack:_ponto1 forPonto:_ponto2];
    _ponto3 = [self setNext:_ponto4 andBack:_ponto2 forPonto:_ponto3];
    _ponto4 = [self setNext:_ponto5 andBack:_ponto3 forPonto:_ponto4];
    _ponto5 = [self setNext:_ponto1 andBack:_ponto4 forPonto:_ponto5];
}

-(void)checkCasa
{
    if( [_ponto1 jaConectado] && [_ponto2 jaConectado] && [_ponto3 jaConectado] && [_ponto4 jaConectado] && [_ponto5 jaConectado])
    {
        [self clearScene];
        [self drawResult];
        [_ponto1 removeFromParent];
        [_ponto2 removeFromParent];
        [_ponto3 removeFromParent];
        [_ponto4 removeFromParent];
        [_ponto5 removeFromParent];
        _estado = 2;
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:1.0f];
    }
}

-(Ponto *)touchCasa:(CGPoint)location
{
    Ponto *alvo = nil;
    
    if([_ponto1 containsPoint:location])
        alvo = _ponto1;
    if([_ponto2 containsPoint:location])
        alvo = _ponto2;
    if([_ponto3 containsPoint:location])
        alvo = _ponto3;
    if([_ponto4 containsPoint:location])
        alvo = _ponto4;
    if([_ponto5 containsPoint:location])
        alvo = _ponto5;
    
    return alvo;
}

#pragma mark ESTRELA

-(void)setupEstrela
{
    _ponto1 = [self setupPonto:_ponto1 withPosition:CGPointMake(480, 65)];
    _ponto2 = [self setupPonto:_ponto1 withPosition:CGPointMake(680,210)];
    _ponto3 = [self setupPonto:_ponto1 withPosition:CGPointMake(875,65)];
    _ponto4 = [self setupPonto:_ponto1 withPosition:CGPointMake(800, 300)];
    _ponto5 = [self setupPonto:_ponto1 withPosition:CGPointMake(1000, 440)];
    _ponto6 = [self setupPonto:_ponto1 withPosition:CGPointMake(750, 440)];
    _ponto7 = [self setupPonto:_ponto1 withPosition:CGPointMake(680, 680)];
    _ponto8 = [self setupPonto:_ponto1 withPosition:CGPointMake(600, 440)];
    _ponto9 = [self setupPonto:_ponto1 withPosition:CGPointMake(360, 440)];
    _ponto10 = [self setupPonto:_ponto1 withPosition:CGPointMake(555, 295)];
    
    _ponto1 = [self setNext:_ponto2 andBack:_ponto10 forPonto:_ponto1];
    _ponto2 = [self setNext:_ponto3 andBack:_ponto1 forPonto:_ponto2];
    _ponto3 = [self setNext:_ponto4 andBack:_ponto2 forPonto:_ponto3];
    _ponto4 = [self setNext:_ponto5 andBack:_ponto3 forPonto:_ponto4];
    _ponto5 = [self setNext:_ponto6 andBack:_ponto4 forPonto:_ponto5];
    _ponto6 = [self setNext:_ponto7 andBack:_ponto5 forPonto:_ponto6];
    _ponto7 = [self setNext:_ponto8 andBack:_ponto6 forPonto:_ponto7];
    _ponto8 = [self setNext:_ponto9 andBack:_ponto7 forPonto:_ponto8];
    _ponto9 = [self setNext:_ponto10 andBack:_ponto8 forPonto:_ponto9];
    _ponto10 = [self setNext:_ponto1 andBack:_ponto9 forPonto:_ponto10];
}

-(void)checkEstrela
{
    if( [_ponto1 jaConectado] && [_ponto2 jaConectado] && [_ponto3 jaConectado] && [_ponto4 jaConectado] && [_ponto5 jaConectado] && [_ponto6 jaConectado] && [_ponto7 jaConectado] && [_ponto8 jaConectado] && [_ponto9 jaConectado] && [_ponto10 jaConectado])
    {
        [self clearScene];
        [self drawResult];
        [_ponto1 removeFromParent];
        [_ponto2 removeFromParent];
        [_ponto3 removeFromParent];
        [_ponto4 removeFromParent];
        [_ponto5 removeFromParent];
        [_ponto6 removeFromParent];
        [_ponto7 removeFromParent];
        [_ponto8 removeFromParent];
        [_ponto9 removeFromParent];
        [_ponto10 removeFromParent];
        _estado = 2;
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:1.0f];
    }
}

-(Ponto *)touchEstrela:(CGPoint)location
{
    Ponto *alvo = nil;
    
    if([_ponto1 containsPoint:location])
        alvo = _ponto1;
    if([_ponto2 containsPoint:location])
        alvo = _ponto2;
    if([_ponto3 containsPoint:location])
        alvo = _ponto3;
    if([_ponto4 containsPoint:location])
        alvo = _ponto4;
    if([_ponto5 containsPoint:location])
        alvo = _ponto5;
    if([_ponto6 containsPoint:location])
        alvo = _ponto6;
    if([_ponto7 containsPoint:location])
        alvo = _ponto7;
    if([_ponto8 containsPoint:location])
        alvo = _ponto8;
    if([_ponto9 containsPoint:location])
        alvo = _ponto9;
    if([_ponto10 containsPoint:location])
        alvo = _ponto10;
    
    return alvo;
}

#pragma mark PEIXE

-(void)setupPeixe
{
    _ponto1 = [self setupPonto:_ponto1 withPosition:CGPointMake(300, 380)];
    _ponto2 = [self setupPonto:_ponto1 withPosition:CGPointMake(585,110)];
    _ponto3 = [self setupPonto:_ponto1 withPosition:CGPointMake(810,320)];
    _ponto4 = [self setupPonto:_ponto1 withPosition:CGPointMake(1010, 200)];
    _ponto5 = [self setupPonto:_ponto1 withPosition:CGPointMake(1010, 580)];
    _ponto6 = [self setupPonto:_ponto1 withPosition:CGPointMake(810, 450)];
    _ponto7 = [self setupPonto:_ponto1 withPosition:CGPointMake(585, 700)];
    
    _ponto1 = [self setNext:_ponto2 andBack:_ponto7 forPonto:_ponto1];
    _ponto2 = [self setNext:_ponto3 andBack:_ponto1 forPonto:_ponto2];
    _ponto3 = [self setNext:_ponto4 andBack:_ponto2 forPonto:_ponto3];
    _ponto4 = [self setNext:_ponto5 andBack:_ponto3 forPonto:_ponto4];
    _ponto5 = [self setNext:_ponto6 andBack:_ponto4 forPonto:_ponto5];
    _ponto6 = [self setNext:_ponto7 andBack:_ponto5 forPonto:_ponto6];
    _ponto7 = [self setNext:_ponto1 andBack:_ponto6 forPonto:_ponto7];
}

-(void)checkPeixe
{
    if( [_ponto1 jaConectado] && [_ponto2 jaConectado] && [_ponto3 jaConectado] && [_ponto4 jaConectado] && [_ponto5 jaConectado] && [_ponto6 jaConectado] && [_ponto7 jaConectado])
    {
        [self clearScene];
        [self drawResult];
        [_ponto1 removeFromParent];
        [_ponto2 removeFromParent];
        [_ponto3 removeFromParent];
        [_ponto4 removeFromParent];
        [_ponto5 removeFromParent];
        [_ponto6 removeFromParent];
        [_ponto7 removeFromParent];
        _estado = 2;
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:1.0f];
    }
}

-(Ponto *)touchPeixe:(CGPoint)location
{
    Ponto *alvo = nil;
    
    if([_ponto1 containsPoint:location])
        alvo = _ponto1;
    if([_ponto2 containsPoint:location])
        alvo = _ponto2;
    if([_ponto3 containsPoint:location])
        alvo = _ponto3;
    if([_ponto4 containsPoint:location])
        alvo = _ponto4;
    if([_ponto5 containsPoint:location])
        alvo = _ponto5;
    if([_ponto6 containsPoint:location])
        alvo = _ponto6;
    if([_ponto7 containsPoint:location])
        alvo = _ponto7;
    
    return alvo;
}

#pragma mark GATO

-(void)setupGato
{
    _ponto1 = [self setupPonto:_ponto1 withPosition:CGPointMake(325, 230)];
    _ponto2 = [self setupPonto:_ponto1 withPosition:CGPointMake(660,50)];
    _ponto3 = [self setupPonto:_ponto1 withPosition:CGPointMake(1000,230)];
    _ponto4 = [self setupPonto:_ponto1 withPosition:CGPointMake(880, 660)];
    _ponto5 = [self setupPonto:_ponto1 withPosition:CGPointMake(770, 530)];
    _ponto6 = [self setupPonto:_ponto1 withPosition:CGPointMake(550, 530)];
    _ponto7 = [self setupPonto:_ponto1 withPosition:CGPointMake(440, 660)];
    
    _ponto1 = [self setNext:_ponto2 andBack:_ponto7 forPonto:_ponto1];
    _ponto2 = [self setNext:_ponto3 andBack:_ponto1 forPonto:_ponto2];
    _ponto3 = [self setNext:_ponto4 andBack:_ponto2 forPonto:_ponto3];
    _ponto4 = [self setNext:_ponto5 andBack:_ponto3 forPonto:_ponto4];
    _ponto5 = [self setNext:_ponto6 andBack:_ponto4 forPonto:_ponto5];
    _ponto6 = [self setNext:_ponto7 andBack:_ponto5 forPonto:_ponto6];
    _ponto7 = [self setNext:_ponto1 andBack:_ponto6 forPonto:_ponto7];
}

-(void)checkGato
{
    if( [_ponto1 jaConectado] && [_ponto2 jaConectado] && [_ponto3 jaConectado] && [_ponto4 jaConectado] && [_ponto5 jaConectado] && [_ponto6 jaConectado] && [_ponto7 jaConectado])
    {
        [self clearScene];
        [self drawResult];
        [_ponto1 removeFromParent];
        [_ponto2 removeFromParent];
        [_ponto3 removeFromParent];
        [_ponto4 removeFromParent];
        [_ponto5 removeFromParent];
        [_ponto6 removeFromParent];
        [_ponto7 removeFromParent];
        _estado = 2;
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:1.0f];
    }
}

-(Ponto *)touchGato:(CGPoint)location
{
    Ponto *alvo = nil;
    
    if([_ponto1 containsPoint:location])
        alvo = _ponto1;
    if([_ponto2 containsPoint:location])
        alvo = _ponto2;
    if([_ponto3 containsPoint:location])
        alvo = _ponto3;
    if([_ponto4 containsPoint:location])
        alvo = _ponto4;
    if([_ponto5 containsPoint:location])
        alvo = _ponto5;
    if([_ponto6 containsPoint:location])
        alvo = _ponto6;
    if([_ponto7 containsPoint:location])
        alvo = _ponto7;
    
    return alvo;
}

-(void)enviaMensagemAcabouLine
{
    [player stop];
    if(_storyDone)
    {
        PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
        scene.storyDone = true;
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene];
    }
    else
    {
        Fim *scene = [Fim sceneWithSize:CGSizeMake(1335, 750)];
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
    }
}

@end







