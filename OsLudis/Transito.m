//
//  Transito.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 30/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Transito.h"
#import "Simulacao.h"

@implementation Transito

-(void)didMoveToView:(SKView *)view
{
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];
    
    _bg = [SKSpriteNode spriteNodeWithImageNamed:@"transito01"];
    _bg.position = CGPointMake(666, 375);
    [self addChild:_bg];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"avanca_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    
    [self performSelector:@selector(toca:) withObject:@"transito01.mp3" afterDelay:1.0f];
    [self performSelector:@selector(changeImage:) withObject:@"transito02" afterDelay:4.5f];
    [self performSelector:@selector(toca:) withObject:@"transito01a.mp3" afterDelay:5.0f];
    [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:8.0f];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        if([_repeat containsPoint:location])
        {
            [_bgMus stop];
            Transito *scene = [Transito sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.3f]];
        }
        if([_next containsPoint:location])
        {
            [_bgMus stop];
            
            Simulacao *scene = [Simulacao sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
        }
    }

}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)changeImage:(NSString *)image
{
    
    SKShapeNode *rect = [SKShapeNode shapeNodeWithRect:CGRectMake(-1, -1, 1336, 753)];
    rect.zPosition = 100;
    rect.fillColor = [SKColor blackColor];
    rect.alpha = 0;
    [rect runAction:[SKAction fadeAlphaTo:1 duration:0.3f]];
    [self addChild:rect];
    NSLog(@"ESCURECEU");
    [self performSelector:@selector(clareia:) withObject:rect afterDelay:0.3f];
    [self performSelector:@selector(muda:) withObject:image afterDelay:0.3f];
}


-(void)clareia:(SKShapeNode *)rect
{
    [rect runAction:[SKAction fadeAlphaTo:0 duration:0.3f]];
    NSLog(@"CLAREOU");
}

-(void)muda:(NSString *)image
{
    _bg.texture = [SKTexture textureWithImageNamed:image];
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

@end
