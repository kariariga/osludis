//
//  Nina.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Nina.h"
#import "Simulacao.h"

@implementation Nina

-(void)didMoveToView:(SKView *)view
{
    _podeAcordar = false;
    
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            _permissao = true;
        }
        else {
            _permissao = false;
        }
    }];
    
    //PREPARACAO DOS INPUTS DO MICROFONE
    
    NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
    
    NSDictionary *settings =  [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNumber numberWithFloat: 44100.0],                 AVSampleRateKey,
                               [NSNumber numberWithInt: kAudioFormatMPEG4AAC],      AVFormatIDKey,
                               [NSNumber numberWithInt: 2],                         AVNumberOfChannelsKey,
                               [NSNumber numberWithInt: AVAudioQualityMax],         AVEncoderAudioQualityKey,
                               nil];
    NSError *error;
    
    recorder = [[AVAudioRecorder alloc]initWithURL:url settings:settings error:&error];
    
    if(recorder)
    {
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
        [recorder record];
    }
    else
    {
        NSLog(@"%@",[error description]);
    }
    
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];
    
    //BACKGROUND
    _bg = [SKSpriteNode spriteNodeWithImageNamed:@"telaNina1"];
    _bg.position = CGPointMake(666, 375);
    _bg.zPosition = -10;
    [self addChild:_bg];
    
    //BLU
    _blu = [SKSpriteNode spriteNodeWithImageNamed:@"headBlu01"];
    _blu.position = CGPointMake(300, 230);
    _blu.alpha = 0;
    [self addChild:_blu];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"avanca_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    //MICROFONE SPRITE
    _microfone = [SKSpriteNode spriteNodeWithImageNamed:@"micro01"];
    _microfone.position = CGPointMake(1800, 100);
    _microfone.xScale = 0.5f;
    _microfone.yScale = 0.5;
    [self addChild:_microfone];
    
    
    [self performSelector:@selector(toca:) withObject:@"nina01.mp3" afterDelay:1.0f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNina2" afterDelay:6.0f];
    [self performSelector:@selector(mostraBlu:) withObject:nil afterDelay:6.0f];
    [self performSelector:@selector(toca:) withObject:@"nina02.mp3" afterDelay:7.0f];
    [self performSelector:@selector(toca:) withObject:@"nina03.mp3" afterDelay:11.0f];
    [self performSelector:@selector(toca:) withObject:@"nina04.mp3" afterDelay:14.0f];
    [self performSelector:@selector(mostraBlu:) withObject:@"headBlu02" afterDelay:14.0f];
    if(_permissao)
        [self performSelector:@selector(toca:) withObject:@"nina05.mp3" afterDelay:17.0f];
    else
        [self performSelector:@selector(toca:) withObject:@"nina05a.mp3" afterDelay:17.0f];
    [self performSelector:@selector(animaMic) withObject:nil afterDelay:17.0f];
    [self performSelector:@selector(deixaAcordar) withObject:nil afterDelay:20.0f];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];

        if([_repeat containsPoint:location])
        {
            Nina *scene = [Nina sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.4f]];
        }
        if([_next containsPoint:location])
        {
            Simulacao *scene = [Simulacao sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
        }
        
        if(_permissao == false)
        {
            if(_podeAcordar)
            {
                if([_microfone containsPoint:location])
                {
                    [self performSelector:@selector(pegouMicrofone) withObject:nil afterDelay:1.0f];
                    _podeAcordar = false;
                }
            }
        }
    }
}

-(void)update:(NSTimeInterval)currentTime
{
    if(_permissao)
    {
        if(_podeAcordar)
        {
            [recorder updateMeters];
            
            if([recorder averagePowerForChannel:0] > -10)
            {
                [self performSelector:@selector(pegouMicrofone) withObject:nil afterDelay:1.0f];
                _podeAcordar = false;
            }
        }
    }
}

-(void)pegouMicrofone
{
    _animandoMicrofone = false;
    [_microfone runAction:[SKAction moveToX:1800 duration:1.0f]];
    [self performSelector:@selector(desmostraBlu) withObject:nil afterDelay:0.0f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNina03a" afterDelay:0.0f];
    [self performSelector:@selector(toca:) withObject:@"nina06.mp3" afterDelay:1.0f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNina03b" afterDelay:1.3f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNina03c" afterDelay:2.5f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNina4" afterDelay:3.2f];
    [self performSelector:@selector(toca:) withObject:@"nina07.mp3" afterDelay:5.0f];
    [self performSelector:@selector(toca:) withObject:@"nina08.mp3" afterDelay:8.0f];
    [self performSelector:@selector(changeImage:) withObject:@"telaNinaFim" afterDelay:8.0f];
    [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:8.0f];
}


-(void)changeImage:(NSString *)nextImage
{
    _bg.texture = [SKTexture textureWithImageNamed:nextImage];
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)deixaAcordar
{
    _podeAcordar = true;
}

-(void)animaMic
{
    [_microfone runAction:[SKAction moveToX:1300 duration:0.2f]];
    if(_permissao)
        [self performSelector:@selector(moveRight) withObject:nil afterDelay:0.3f];
    else
    {
        _microfone.xScale = .7f;
        _microfone.yScale = .7f;
    }
    _animandoMicrofone = true;
}

-(void)mostraBlu:(NSString *)image
{
    if(image)
    {
        _blu.texture = [SKTexture textureWithImageNamed:image];
    }
    else
    {
        _blu.alpha = 1;
    }
}

-(void)desmostraBlu
{
    _blu.alpha = 0;
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

-(void)moveRight
{
    [_microfone runAction:[SKAction moveToX:_microfone.position.x + 50 duration:0.5f]];
    if(_animandoMicrofone)
        [self performSelector:@selector(moveLeft) withObject:nil afterDelay:0.5f];
}

-(void)moveLeft
{
    [_microfone runAction:[SKAction moveToX:_microfone.position.x - 50 duration:0.5f]];
    if(_animandoMicrofone)
        [self performSelector:@selector(moveRight) withObject:nil afterDelay:0.5f];
}

@end
