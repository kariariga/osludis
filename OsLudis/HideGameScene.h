//
//  HideGameScene.h
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 21/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@interface HideGameScene : SKScene
{
    AVAudioPlayer *player;
}

@property bool storyDone;

@property int acertou;

@property int estado;

@property SKSpriteNode *playAgain;
@property SKSpriteNode *goOn;

-(void)randomize;

-(bool)doesArray:(NSArray *)array has:(int)num;

@property bool esperando;

@property SKSpriteNode *char1;
@property SKSpriteNode *char2;
@property SKSpriteNode *char3;
@property SKSpriteNode *char4;
@property SKSpriteNode *char5;

@property SKShapeNode *rect;

@property bool jaAvancou;

@end
