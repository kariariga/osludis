//
//  LineGameScene.h
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Ponto.h"

@interface LineGameScene : SKScene
{
    AVAudioPlayer *player;
}

@property bool storyDone;

@property int estado;

@property SKSpriteNode *playAgain;
@property SKSpriteNode *goOn;

@property SKSpriteNode *mao;

@property SKShapeNode *yourLine;

@property bool drawning;
@property Ponto *origin;

@property CGPoint lineDown;
@property CGPoint lineMove;

@property int fase;

@property Ponto *ponto1;
@property Ponto *ponto2;
@property Ponto *ponto3;
@property Ponto *ponto4;
@property Ponto *ponto5;
@property Ponto *ponto6;
@property Ponto *ponto7;
@property Ponto *ponto8;
@property Ponto *ponto9;
@property Ponto *ponto10;
@property Ponto *ponto11;
@property Ponto *ponto12;
@property Ponto *ponto13;
@property Ponto *ponto14;
@property Ponto *ponto15;
@property Ponto *ponto16;

@property bool jaAvancou;

-(void)configure;
-(Ponto *)setupPonto:(Ponto *)ponto withPosition:(CGPoint)position;

@end
