//
//  Atravessador.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Atravessador.h"

@implementation Atravessador

-(int)possoAtravesar:(CGPoint)location
{
    if(_atravessando)
        return 0;
    
    if([self containsPoint:location])
    {
        if(_mySema.aberto)
        {
            _atravessando = true;
            _mySema.atravessando = true;
            return 1;
        }
        else
        {
            return 2;
        }
    }
    return 0;
}

-(int)update
{
    if(_atravessando)
    {
        self.position = CGPointMake(self.position.x, self.position.y + 10);
        self.xScale *= 0.993;
        self.yScale *= 0.993;
        
        if(self.position.y > 680)
            _mySema.atravessando = false;
        
        if(self.position.y > 760)
        {
            int r = arc4random()%3;
            
            switch(r)
            {
                case 0:
                    self.texture = [SKTexture textureWithImageNamed:@"p1"];
                    break;
                case 1:
                    self.texture = [SKTexture textureWithImageNamed:@"p2"];
                    break;
                case 2:
                    self.texture = [SKTexture textureWithImageNamed:@"p3"];
                    break;
            }
            
            self.atravessando = false;
            self.xScale = 1;
            self.yScale = 1;
            self.position = CGPointMake(self.position.x, -30);
            [self runAction:[SKAction moveToY:50 duration:0.5f]];
            return 1;
        }
    }
    return 0;
}

@end
