//
//  Fim.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Fim : SKScene

@property AVAudioPlayer *bgMus;

@property SKSpriteNode *repeat;
@property SKSpriteNode *next;

@end