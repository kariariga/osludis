//
//  GameScene.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "GameScene.h"
#import "Recreio.h"
#import "PrincipalScene.h"

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    _jaAvancou = false;
    
    _estado = 1;
    _playAgain = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    if(_storyDone)
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"home_batata"];
    else
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"play_batata"];
    
    _playAgain.position = CGPointMake(400, 900);
    _playAgain.zPosition = 400;
    _goOn.position = CGPointMake(934, 900);
    _goOn.zPosition = 400;

    NSString *tema= [[NSBundle mainBundle]pathForResource:@"theme" ofType:@"mp3"];
    _theme = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _theme.numberOfLoops = -1;
    [_theme play];
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"fundoCard"];
    bg.zPosition = -1000;
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
    
    _initLocation.x = 666;
    _initLocation.y = 100;
    
    if(_passados == 0)
        [self runAction:[SKAction playSoundFileNamed:@"braille01a.mp3" waitForCompletion:NO]];
}

-(void)configure
{
     [self setupGame:_letters withRandom:_shuffled];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if(_estado == 1)
        {
            if([_carta1 containsPoint:location])
                [_carta1 onDrag:location];
            if([_carta2 containsPoint:location])
                [_carta2 onDrag:location];
            if([_carta3 containsPoint:location])
                [_carta3 onDrag:location];
            if([_carta4 containsPoint:location])
                [_carta4 onDrag:location];
        }
        else
        {
            if([_playAgain containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    CGSize size;
                    size.width = 1334;
                    size.height = 750;
                    GameScene *scene = [GameScene sceneWithSize:size];
                    NSArray *letters = @[ @"F", @"O", @"C",@"A"];
                    NSArray *shuffled= @[ @3, @2, @1, @0];
                    scene.letters = letters;
                    scene.shuffled = shuffled;
                    scene.passados = 0;
                    [scene configure];
                    scene.storyDone = [self storyDone];
                    [_theme stop];
                    scene.scaleMode = SKSceneScaleModeAspectFit;
                    [self.view presentScene:scene transition:[SKTransition doorsCloseHorizontalWithDuration:1.0f]];
                    _jaAvancou = true;
                }
            }
            if([_goOn containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    [_theme stop];
                    if([self storyDone] == false)
                    {
                        [self removeAllChildren];
                        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"braille02.png"];
                        [self runAction:[SKAction playSoundFileNamed:@"sirene.wav" waitForCompletion:NO]];
                        bg.position = CGPointMake(666, 375);
                        [self addChild:bg];
                        _estado = 40;
                        [self performSelector:@selector(enviaTerminouCard) withObject:nil afterDelay:2.0f];
                    }
                    else
                    {
                        [self enviaTerminouCard];
                    }
                    _jaAvancou = true;
                }
            }
        }
    }
    

}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        [_carta1 updatePosition:location];
        [_carta2 updatePosition:location];
        [_carta3 updatePosition:location];
        [_carta4 updatePosition:location];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    Slot *slot = nil;
    
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if([_slot1 containsPoint:location])
            slot = _slot1;
        if([_slot2 containsPoint:location])
            slot = _slot2;
        if([_slot3 containsPoint:location])
            slot = _slot3;
        if([_slot4 containsPoint:location])
            slot = _slot4;
        
        [_carta1 onDrop:slot];
        [_carta2 onDrop:slot];
        [_carta3 onDrop:slot];
        [_carta4 onDrop:slot];
        
    }
    
    if(_carta1.locked && _carta2.locked && _carta3.locked && _carta4.locked)
    {
        [_letra1 goTodestination];
        [_letra2 goTodestination];
        [_letra3 goTodestination];
        [_letra4 goTodestination];
        
        [self performSelector:@selector(removeSelf) withObject:nil afterDelay:2.0f];
    }
    
    
}

-(void)removeSelf
{
    if(_passados < 4)
    {
        CGSize size;
        size.width = 1334;
        size.height = 750;
        GameScene *scene = [GameScene sceneWithSize:size];
        scene.passados = _passados+1;
        scene.storyDone = [self storyDone];
        
        NSArray *letters;
        NSArray *shuffled;

        switch(_passados)
        {
            case 0:
                letters = @[@"G",@"A", @"T", @"O"];
                shuffled = @[ @3, @1, @0, @2];
                break;
            case 1:
                letters = @[ @"B", @"O", @"L", @"A"];
                shuffled = @[ @0, @3, @2, @1];
                break;
            case 2:
                letters = @[ @"A", @"N", @"E", @"L"];
                shuffled = @[ @1, @3, @2, @0];
                break;
            case 3:
                letters = @[ @"F", @"L", @"O", @"R"];
                shuffled = @[ @2, @1, @0, @3];
                break;
        }
        scene.letters = letters;
        scene.shuffled = shuffled;
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [scene configure];
        [_theme stop];
        [self.view presentScene:scene transition:[SKTransition doorsCloseHorizontalWithDuration:0.5f]];
        
    }
    else
    {
        _estado = 2;
        [self addChild:_playAgain];
        [self addChild:_goOn];
        SKShapeNode *rec = [SKShapeNode shapeNodeWithRect:CGRectMake(-5, -5, 1500, 800)];
        rec.alpha = 0;
        rec.zPosition = 300;
        rec.fillColor = [SKColor blackColor];
        [rec runAction:[SKAction fadeAlphaTo:.8 duration:0.5f]];
        [self addChild:rec];
        
        [_playAgain runAction:[SKAction moveToY:375 duration:1.0f]];
        [_goOn runAction:[SKAction moveToY:375 duration:1.0f]];
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}


-(Card *)setupCard:(Card *)card withName:(NSString *)image andPosition:(CGPoint)position
{
    card = [Card spriteNodeWithImageNamed:image];
    card.position = position;
    card.dragging = false;
    card.locked = false;
    card.xScale = .6;
    card.yScale = .6;
    card.value = image;
    card.initialPosition = position;
    [self addChild:card];
    return card;
    
}


-(void)setupGame:(NSArray *)letters withRandom:(NSArray *)shuffled
{
    
    NSString *slot1 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:0]];
    NSString *slot2 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:1]];
    NSString *slot3 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:2]];
    NSString *slot4 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:3]];
    
    NSString *card1 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:[[shuffled objectAtIndex:0] integerValue]]];
    NSString *card2 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:[[shuffled objectAtIndex:1] integerValue]]];
    NSString *card3 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:[[shuffled objectAtIndex:2] integerValue]]];
    NSString *card4 = [NSString stringWithFormat:@"card%@",[letters objectAtIndex:[[shuffled objectAtIndex:3] integerValue]]];
    
    CGPoint point1 = CGPointMake(400,600);
    CGPoint point2 = CGPointMake(600,600);
    CGPoint point3 = CGPointMake(800,600);
    CGPoint point4 = CGPointMake(1000,600);

    _slot1 = [self setupSlot:_slot1 withName:slot1 andPosition:point1];
    _slot2 = [self setupSlot:_slot2 withName:slot2 andPosition:point2];
    _slot3 = [self setupSlot:_slot3 withName:slot3 andPosition:point3];
    _slot4 = [self setupSlot:_slot4 withName:slot4 andPosition:point4];
    
    _carta4 = [self setupCard:_carta4 withName:card1 andPosition:CGPointMake(250, 200)];
    _carta2 = [self setupCard:_carta2 withName:card2 andPosition:CGPointMake(550, 200)];
    _carta1 = [self setupCard:_carta1 withName:card3 andPosition:CGPointMake(850, 200)];
    _carta3 = [self setupCard:_carta3 withName:card4 andPosition:CGPointMake(1150, 200)];
    
    
    NSMutableArray *arrayLetras = [[NSMutableArray alloc]init];
    
    for(int i=0;i<4;i++)
    {
        CGPoint destiny;
        CGPoint position;
        switch([[shuffled objectAtIndex:i] integerValue])
        {
            case 0:
                destiny = CGPointMake(point1.x, 200);
                break;
            case 1:
                destiny =CGPointMake(point2.x, 200);
                break;
            case 2:
                destiny = CGPointMake(point3.x, 200);
                break;
            case 3:
                destiny =CGPointMake(point4.x, 200);
                break;
        }
        switch(i)
        {
            case 0:
                position = CGPointMake(100, 130);
                break;
            case 1:
                position = CGPointMake(400, 130);
                break;
            case 2:
                position = CGPointMake(700,130);
                break;
            case 3:
                position = CGPointMake(1000, 130);
                break;
        }
        NSString *image = [letters objectAtIndex:[[shuffled objectAtIndex:i]intValue]];
        Letra *letra = [[Letra alloc]init];
        letra = [self setupLetra:letra withName:image andPosition:position  adnDestination:destiny];
        [arrayLetras addObject:letra];
        
    }
    
    _letra1 = [arrayLetras objectAtIndex:0];
    _letra2 = [arrayLetras objectAtIndex:1];
    _letra3 = [arrayLetras objectAtIndex:2];
    _letra4 = [arrayLetras objectAtIndex:3];
}


-(Slot *)setupSlot:(Slot *)slot withName:(NSString *)image andPosition:(CGPoint)position
{
    slot = [Slot spriteNodeWithImageNamed:image];
    slot.zPosition = -10;
    slot.alpha = .5;
    slot.position = position;
    slot.xScale = .6;
    slot.yScale = .6;
    slot.value = image;
    [self addChild:slot];
    return slot;
}

-(Letra *)setupLetra:(Letra *)letra withName:(NSString *)image andPosition:(CGPoint)position adnDestination:(CGPoint)destination
{
    letra = [Letra spriteNodeWithImageNamed:image];
    letra.zPosition = -1;
    letra.position = position;
    letra.destinationPoint = destination;
    letra.xScale = 1;
    letra.yScale = 1;
    [self addChild:letra];
    return letra;
}

-(void)enviaTerminouCard
{
    [_theme stop];
    if(_storyDone)
    {
        PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
        scene.storyDone = true;
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
    }
    else
    {
        [_playAgain removeFromParent];
        [_goOn removeFromParent];
        Recreio *scene = [Recreio sceneWithSize:CGSizeMake(1335, 750)];
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
    }
}

@end
