//
//  Semaforo.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Semaforo.h"

@implementation Semaforo

-(void)abreSemaforo
{
    _aberto = true;
    self.texture = [SKTexture textureWithImageNamed:@"green"];
}

-(void)fechaSemaforo
{
    _aberto = false;
    self.texture = [SKTexture textureWithImageNamed:@"red"];
}

@end
