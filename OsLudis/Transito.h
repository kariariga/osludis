//
//  Transito.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 30/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Transito : SKScene <AVAudioPlayerDelegate>

@property AVAudioPlayer *bgMus;

@property SKSpriteNode *bg;

@property SKSpriteNode *repeat;
@property SKSpriteNode *next;

@end
