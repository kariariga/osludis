//
//  Ponto.h
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Ponto : SKSpriteNode

@property Ponto *next;
@property Ponto *back;

@property bool conectouNext;
@property bool conectouback;


-(void)conect:(Ponto *)ponto;
-(bool)jaConectado;


@end
