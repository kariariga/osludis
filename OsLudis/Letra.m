//
//  Letra.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "Letra.h"

@implementation Letra

-(void)goTodestination
{
    SKAction *action = [SKAction moveTo:_destinationPoint duration:.5f];
    SKAction *action2 = [SKAction scaleTo:1.6f duration:.5f];
    [self runAction:action];
    [self runAction:action2];
}

@end
