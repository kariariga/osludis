//
//  carta.h
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 17/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Slot.h"

@interface Card : SKSpriteNode

@property NSString *value;

@property bool dragging;

@property bool locked;

@property CGPoint offset;

@property CGPoint initialPosition;


-(void)onDrag:(CGPoint)location;
-(void)updatePosition:(CGPoint)location;
-(void)onDrop:(Slot *)slot;

@end
