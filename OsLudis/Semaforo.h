//
//  Semaforo.h
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 25/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Semaforo : SKSpriteNode

@property bool aberto;

@property bool atravessando;

-(void)abreSemaforo;

-(void)fechaSemaforo;

@end
