//
//  GameScene.m
//  crossStreet
//
//  Created by Ricardo Striffler on 20/04/15.
//  Copyright (c) 2015 Ricardo Striffler. All rights reserved.
//

#import "StreetGameScene.h"
#import "PrincipalScene.h"
#import "Braille.h"

@implementation StreetGameScene

-(void)didMoveToView:(SKView *)view
{
    
    _jaAvancou = false;
    
    _estado = 1;
    _playAgain = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    
    if([self storyDone])
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"home_batata"];
    else
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"play_batata"];
    
    _playAgain.position = CGPointMake(400, 900);
    _playAgain.zPosition = 400;
    _goOn.position = CGPointMake(934, 900);
    _goOn.zPosition = 400;

    
    //BACKGROUND
    
    _background = [SKSpriteNode spriteNodeWithImageNamed:@"fundo"];
    _background.position = CGPointMake(666, 375);
    _background.zPosition = -100;
    [self addChild:_background];
    
    
    //SEMAFOROS
    
    _semaforo1 = [Semaforo spriteNodeWithImageNamed:@"red"];
    _semaforo1.position = CGPointMake(300, 650);
    _semaforo1.atravessando = false;
    _semaforo2 = [Semaforo spriteNodeWithImageNamed:@"red"];
    _semaforo2.position = CGPointMake(770, 650);
    _semaforo2.atravessando = false;
    _semaforo3 = [Semaforo spriteNodeWithImageNamed:@"red"];
    _semaforo3.position = CGPointMake(1210, 650);
    _semaforo3.atravessando = false;

    
    [self fechaSemaforo1];
    [self fechaSemaforo2];
    [self fechaSemaforo3];
    
    [self addChild:_semaforo1];
    [self addChild:_semaforo2];
    [self addChild:_semaforo3];
    
    //CARROS
    
    _carroDir = [CarroDir spriteNodeWithImageNamed:@"carro1"];
    _carroDir.nextX = 0;
    _carroDir.position = CGPointMake(-300, 233);
    _carroDir.xScale = .8f;
    _carroDir.yScale = .8f;
    
    _carroDir.ponto = 1;
    _carroDir.sema1 = _semaforo1;
    _carroDir.sema2 = _semaforo2;
    _carroDir.sema3 = _semaforo3;
    
    [_carroDir runAction:[SKAction moveToX:_carroDir.nextX duration:0.5]];
    
    _carroEsq = [CarroEsq spriteNodeWithImageNamed:@"carro3"];
    _carroEsq.nextX = 1360;
    _carroEsq.xScale = .8f;
    _carroEsq.yScale = .8f;
    
    _carroEsq.ponto = 1;
    _carroEsq.sema1 = _semaforo3;
    _carroEsq.sema2 = _semaforo2;
    _carroEsq.sema3 = _semaforo1;
    
    _carroEsq.position = CGPointMake(1500, 450);
    [_carroEsq runAction:[SKAction moveToX:_carroEsq.nextX duration:0.5]];
    
    [self addChild:_carroDir];
    [self addChild:_carroEsq];
    
    //ATRAVESSADORES
    _atravessador1 = [Atravessador spriteNodeWithImageNamed:@"p1"];
    _atravessador1.mySema = _semaforo1;
    _atravessador1.position = CGPointMake(184, -30);
    [_atravessador1 runAction:[SKAction moveToY:50 duration:0.5f]];
    
    _atravessador2 = [Atravessador spriteNodeWithImageNamed:@"p2"];
    _atravessador2.mySema = _semaforo2;
    _atravessador2.position = CGPointMake(675, -30);
    [_atravessador2 runAction:[SKAction moveToY:50 duration:0.5f]];
    
    _atravessador3 = [Atravessador spriteNodeWithImageNamed:@"p3"];
    _atravessador3.mySema = _semaforo3;
    _atravessador3.position = CGPointMake(1160, -30);
    [_atravessador3 runAction:[SKAction moveToY:50 duration:0.5f]];
    
    [self addChild:_atravessador1];
    [self addChild:_atravessador2];
    [self addChild:_atravessador3];
    
    //CONTROLE VIDAS
    
    _life = 3;

    _life1 = [SKSpriteNode spriteNodeWithImageNamed:@"life"];
    _life1.position = CGPointMake(35, 700);
    _life1.scale = 0.8;
    
    _life2 = [SKSpriteNode spriteNodeWithImageNamed:@"life"];
    _life2.position = CGPointMake(35, 650);
    _life2.scale = 0.8;
    
    _life3 = [SKSpriteNode spriteNodeWithImageNamed:@"life"];
    _life3.position = CGPointMake(35, 600);
    _life3.scale = 0.8;
    
    [self addChild:_life1];
    [self addChild:_life2];
    [self addChild:_life3];
    
    //SOM ERROU
    
    NSString *errouSom= [[NSBundle mainBundle]pathForResource:@"errou" ofType:@"wav"];
    _errou = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:errouSom]error:NULL];
    //_errou.delegate = self;
    _errou.numberOfLoops = 0;
    
    //SOM TEMA
    
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"theme" ofType:@"mp3"];
    _theme = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _theme.numberOfLoops = -1;
    [_theme play];
    
    //CONDIÇÃO VITÓRIA
    _contWin = 0;
    
    //CONTADOR CORRIDAS
    _labelContador = [SKLabelNode labelNodeWithFontNamed:@"Contador Corridas"];
    _labelContador.text = [NSString stringWithFormat:@"%d/9", _contWin];
    _labelContador.fontColor = [UIColor blackColor];
    _labelContador.position = CGPointMake(1290, 650);
    _labelContador.zPosition = 100;
    //_labelContador.fontSize = 15;
    _labelContador.fontName = @"Helvetica";
    
    [self addChild: _labelContador];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        
        if(_estado == 1)
        {
            int ret1 = [_atravessador1 possoAtravesar:location];
            int ret2 = [_atravessador2 possoAtravesar:location];
            int ret3 = [_atravessador3 possoAtravesar:location];
            
            if(ret1 != 0)
            {
                if(ret1 == 1)
                {
                    _atravessador1.atravessando = true;
                }
                else
                {
                    _life--;
                    [_errou play];
                    
                }
            }
            
            if(ret2 != 0)
            {
                if(ret2 == 1)
                {
                    _atravessador2.atravessando = true;
                }
                else
                {
                    _life--;
                    [_errou play];
                    
                }
            }
            
            if(ret3 != 0)
            {
                if(ret3 == 1)
                {
                    _atravessador3.atravessando = true;
                }
                else
                {
                    _life--;
                    [_errou play];
                    
                }
            }
        }
        if(_estado == 2)
        {
            if([_playAgain containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    CGSize size;
                    size.width = 1334;
                    size.height = 750;
                    [_theme stop];
                    
                    StreetGameScene *scene = [StreetGameScene sceneWithSize:size];
                    scene.storyDone = [self storyDone];
                    
                    scene.scaleMode = SKSceneScaleModeAspectFit;                [self.view presentScene:scene transition:[SKTransition doorsCloseHorizontalWithDuration:1.0f]];
                    _jaAvancou = true;
                }
            }
            if([_goOn containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    [_theme stop];
                    if([self storyDone] == false)
                    {
                        [self removeAllChildren];
                        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"fimTransito.png"];
                        bg.position = CGPointMake(666, 375);
                        [self addChild:bg];
                        _estado = 40;
                        [self performSelector:@selector(enviaMensagemAcabouStreet) withObject:nil afterDelay:2.0f];
                    }
                    else
                    {
                        [self enviaMensagemAcabouStreet];
                    }
                    _jaAvancou = true;
                }
            }
        }
    }
}

-(void)update:(NSTimeInterval)currentTime
{
    if(_estado == 1)
    {
        [_carroEsq update];
        [_carroDir update];
        
        if([_atravessador1 update] == 1)
            _contWin++;
        if([_atravessador2 update] == 1)
            _contWin++;;
        if([_atravessador3 update] == 1)
            _contWin++;;
        
        [self controladorVidas];
        
        [self updateLabelContador];
        
        if(_contWin == 9)
        {
            _estado = 2;
            [self addChild:_playAgain];
            [self addChild:_goOn];
            SKShapeNode *rec = [SKShapeNode shapeNodeWithRect:CGRectMake(-5, -5, 1500, 800)];
            rec.alpha = 0;
            rec.zPosition = 300;
            rec.fillColor = [SKColor blackColor];
            [rec runAction:[SKAction fadeAlphaTo:.8 duration:0.5f]];
            [self addChild:rec];
            
            [_playAgain runAction:[SKAction moveToY:375 duration:1.0f]];
            [_goOn runAction:[SKAction moveToY:375 duration:1.0f]];
        }
    }

}

#pragma mark SEMAFORO 1
-(void)abreSemaforo1
{
    [_semaforo1 abreSemaforo];
    [self performSelector:@selector(fechaSemaforo1) withObject:nil afterDelay:5.0f];
}

-(void)fechaSemaforo1
{
    [_semaforo1 fechaSemaforo];
    float delay = 3.0f + arc4random()%5;
    [self performSelector:@selector(abreSemaforo1) withObject:nil afterDelay:delay];
}

#pragma mark SEMAFORO 2
-(void)abreSemaforo2
{
    [_semaforo2 abreSemaforo];
    [self performSelector:@selector(fechaSemaforo2) withObject:nil afterDelay:5.0f];
}

-(void)fechaSemaforo2
{
    [_semaforo2 fechaSemaforo];
    float delay = 3.0f + arc4random()%5;
    [self performSelector:@selector(abreSemaforo2) withObject:nil afterDelay:delay];
}


#pragma mark SEMAFORO 3
-(void)abreSemaforo3
{
    [_semaforo3 abreSemaforo];
    [self performSelector:@selector(fechaSemaforo3) withObject:nil afterDelay:5.0f];
}

-(void)fechaSemaforo3
{
    [_semaforo3 fechaSemaforo];
    float delay = 3.0f + arc4random()%5;
    [self performSelector:@selector(abreSemaforo3) withObject:nil afterDelay:delay];
}

#pragma mark VIDAS

-(void)controladorVidas
{
    switch(_life)
    {
        case 3:
            _life1.texture = [SKTexture textureWithImageNamed:@"life"];
            _life2.texture = [SKTexture textureWithImageNamed:@"life"];
            _life3.texture = [SKTexture textureWithImageNamed:@"life"];
            
            break;
        case 2:
            _life1.texture = [SKTexture textureWithImageNamed:@"life"];
            _life2.texture = [SKTexture textureWithImageNamed:@"life"];
            _life3.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];
            break;
        case 1:
            _life1.texture = [SKTexture textureWithImageNamed:@"life"];
            _life2.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];
            _life3.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];
            break;
        case 0:
            _life1.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];
            _life2.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];
            _life3.texture = [SKTexture textureWithImageNamed:@"lifeEmpty"];

            [self gameOver];
            break;
    }

}

#pragma mark LABEL CONTADOR PASSAGEM
-(void)updateLabelContador
{
    _labelContador.text = [NSString stringWithFormat:@"%d/9", _contWin];
}

#pragma mark GAME OVER
-(void)gameOver
{
    [_theme stop];
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:1.0f],
                                         [SKAction runBlock:^{
                                                            SKTransition *reveal = [SKTransition flipVerticalWithDuration:0.5];
                                                            SKScene * myScene = [[StreetGameScene alloc] initWithSize:self.size];
                                                            myScene.scaleMode = SKSceneScaleModeAspectFit;
                                                            [self.view presentScene:myScene transition: reveal];
                                                                                                                }]]]
                    ];
    
    
}

-(void)enviaMensagemAcabouStreet
{
    [_theme stop];
    if(_storyDone)
    {
        PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
        scene.storyDone = true;
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene];
    }
    else
    {
        Braille *scene = [Braille sceneWithSize:CGSizeMake(1335, 750)];
        scene.scaleMode = SKSceneScaleModeAspectFit;
        [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:.04f]];
    }
}


@end
