//
//  Braille.m
//  OsLudis
//
//  Created by Andre Ferreira dos Santos on 28/04/15.
//  Copyright (c) 2015 Karina Ariga de Oliveira. All rights reserved.
//

#import "Braille.h"
#import "GameScene.h"

@implementation Braille

-(void)didMoveToView:(SKView *)view
{
    //MUSICA
    NSString *tema= [[NSBundle mainBundle]pathForResource:@"intro" ofType:@"mp3"];
    _bgMus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:tema]error:NULL];
    //_theme.delegate = self;
    _bgMus.numberOfLoops = -1;
    [_bgMus play];

    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"braille01"];
    bg.position = CGPointMake(666, 375);
    [self addChild:bg];
    
    //REPEAT
    _repeat = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    _repeat.position = CGPointMake(100, -1000);
    _repeat.zPosition = 2;
    _repeat.xScale = .4;
    _repeat.yScale = .4;
    [self addChild:_repeat];
    
    //NEXT
    _next = [SKSpriteNode spriteNodeWithImageNamed:@"avanca_batata"];
    _next.position = CGPointMake(1234, -1000);
    _next.zPosition = 2;
    _next.xScale = .4;
    _next.yScale = .4;
    [self addChild:_next];
    
    [self performSelector:@selector(toca:) withObject:@"braille01.mp3" afterDelay:0.5f];
    [self performSelector:@selector(mostraBotoes) withObject:nil afterDelay:6.0f];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if([_repeat containsPoint:location])
        {
            [_bgMus stop];
            Braille *scene = [Braille sceneWithSize:CGSizeMake(1335, 750)];
            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition fadeWithColor:[UIColor blackColor] duration:0.3f]];
        }
        if([_next containsPoint:location])
        {
            [_bgMus stop];
            GameScene *scene = [GameScene sceneWithSize:CGSizeMake(1335, 750)];
            NSArray *letters = @[ @"F", @"O", @"C",@"A"];
            NSArray *shuffled= @[ @3, @2, @1, @0];
            scene.letters = letters;
            scene.shuffled = shuffled;
            scene.passados = 0;
            [scene configure];

            scene.scaleMode = SKSceneScaleModeAspectFit;
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:0.4f]];
        }
    }
}

-(void)toca:(NSString *)raul
{
    [self runAction:[SKAction playSoundFileNamed:raul waitForCompletion:NO]];
}

-(void)mostraBotoes
{
    [_repeat runAction:[SKAction moveToY:100 duration:0.5f]];
    [_next runAction:[SKAction moveToY:100 duration:0.5f]];
}

@end
