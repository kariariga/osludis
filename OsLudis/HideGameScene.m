//
//  HideGameScene.m
//  teste Drag and Drop
//
//  Created by Andre Ferreira dos Santos on 21/04/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "HideGameScene.h"
#import "PrincipalScene.h"
#import "Pontos.h"

@implementation HideGameScene

-(void)didMoveToView:(SKView *)view
{
    
    _jaAvancou = false;
    
    _estado = 0;
    _acertou = 0;
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"theme"
                                         ofType:@"mp3"]];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.numberOfLoops = -1;
    
    [player play];
    
    _estado = 1;
    _playAgain = [SKSpriteNode spriteNodeWithImageNamed:@"repetir_batata"];
    if([self storyDone])
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"home_batata"];
    else
        _goOn = [SKSpriteNode spriteNodeWithImageNamed:@"play_batata"];
    
    _playAgain.position = CGPointMake(400, 900);
    _playAgain.zPosition = 400;
    _goOn.position = CGPointMake(934, 900);
    _goOn.zPosition = 400;
    
    

    
    if(_esperando)
    {
        _rect = [SKShapeNode shapeNodeWithRect:CGRectMake(-4,-4,1400,800)];
        _rect.fillColor = [SKColor blackColor];
        _rect.zPosition = 200;
        [_rect runAction:[SKAction playSoundFileNamed:@"esconde04.m4a" waitForCompletion:NO]];
        [self addChild:_rect];
        [self performSelector:@selector(randomize) withObject:nil afterDelay:1.5f];
        [self performSelector:@selector(tocaPodeVirar) withObject:nil afterDelay:4.0f];
    }
    else
    {
        _estado = 1;
        [self randomize];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if(_estado == 1)
        {
            if(location.y > 600)
                [self randomize];
            else
            {
                if([_char1 containsPoint:location])
                {
                    [_char1 removeFromParent];
                    [self runAction:[SKAction playSoundFileNamed:@"intro04.mp3" waitForCompletion:NO]];
                    _char1.position = CGPointMake(-100, -100);
                    _acertou += 1;
                }
                if([_char2 containsPoint:location])
                {
                    [_char2 removeFromParent];
                    [self runAction:[SKAction playSoundFileNamed:@"intro04.mp3" waitForCompletion:NO]];
                    _char2.position = CGPointMake(-100, -100);
                    _acertou += 1;
                }
                if([_char3 containsPoint:location])
                {
                    [_char3 removeFromParent];
                    [self runAction:[SKAction playSoundFileNamed:@"intro04.mp3" waitForCompletion:NO]];
                    _char3.position = CGPointMake(-100, -100);
                    _acertou += 1;
                }
                if([_char4 containsPoint:location])
                {
                    [_char4 removeFromParent];
                    [self runAction:[SKAction playSoundFileNamed:@"intro04.mp3" waitForCompletion:NO]];
                    _char4.position = CGPointMake(-100, -100);
                    _acertou += 1;
                }
                if([_char5 containsPoint:location])
                {
                    [_char5 removeFromParent];
                    [self runAction:[SKAction playSoundFileNamed:@"intro04.mp3" waitForCompletion:NO]];
                    _char5.position = CGPointMake(-100, -100);
                    _acertou += 1;
                }
                
                if(_acertou >= 5)
                {
                    _estado = 2;
                    [self addChild:_playAgain];
                    [self addChild:_goOn];
                    SKShapeNode *rec = [SKShapeNode shapeNodeWithRect:CGRectMake(-5, -5, 1500, 800)];
                    rec.alpha = 0;
                    rec.zPosition = 300;
                    rec.fillColor = [SKColor blackColor];
                    [rec runAction:[SKAction fadeAlphaTo:.8 duration:0.3f]];
                    [self addChild:rec];
                    
                    [_playAgain runAction:[SKAction moveToY:375 duration:0.5f]];
                    [_goOn runAction:[SKAction moveToY:375 duration:0.5f]];
                }
            }
        }
        else if(_estado == 2)
        {
            if([_playAgain containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    CGSize size;
                    size.width = 1334;
                    size.height = 750;
                    HideGameScene *scene = [HideGameScene sceneWithSize:size];
                    scene.esperando = false;
                    scene.storyDone = [self storyDone];
                    scene.scaleMode = SKSceneScaleModeAspectFit;
                    [self.view presentScene:scene transition:[SKTransition doorsCloseHorizontalWithDuration:0.5f]];
                    _jaAvancou = true;
                }
            }
            if([_goOn containsPoint:location])
            {
                if(_jaAvancou == false)
                {
                    [player stop];
                    if(_storyDone)
                    {
                        PrincipalScene *scene = [PrincipalScene sceneWithSize:CGSizeMake(1335, 750)];
                        scene.storyDone = true;
                        scene.scaleMode = SKSceneScaleModeAspectFill;
                        [self.view presentScene:scene];
                        
                    }
                    else
                    {
                        Pontos *scene = [Pontos sceneWithSize:CGSizeMake(1335, 750)];
                        scene.scaleMode = SKSceneScaleModeAspectFit;
                        [self.view presentScene:scene transition:[SKTransition doorsCloseVerticalWithDuration:0.4f]];
                    }
                    _jaAvancou = true;
                }
            }
        }
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)update:(NSTimeInterval)currentTime
{
    if(_acertou >= 5)
        _estado = 2;
}

-(void)randomize
{
    
    [self removeAllChildren];
    _acertou = 0;
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"fundoHide"];
    bg.zPosition = -100;
    bg.position = CGPointMake(666, 375);
    bg.size = CGSizeMake(1335, 750);
    [self addChild:bg];
    
    NSMutableDictionary *itens = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *chars = [[NSMutableDictionary alloc]init];
    NSMutableArray *positions = [[NSMutableArray alloc]init];
    
    for(int i=0;i<5;i++)
    {
        int ran = arc4random()%12;
        NSNumber *r = [[NSNumber alloc]initWithInt:ran];
        
        while ([self doesArray:positions has:ran])
        {
            ran = arc4random()%12;
            r = [[NSNumber alloc]initWithInt:ran];
        }
        
        [positions setObject:r atIndexedSubscript:i];
    }
    
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arbusto1"] forKey:@"arbusto1"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arbusto2"] forKey:@"arbusto2"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arbusto3"] forKey:@"arbusto3"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arbusto4"] forKey:@"arbusto4"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arvore1"] forKey:@"arvore1"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"arvore2"] forKey:@"arvore2"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"balanco"] forKey:@"balanco"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"banco1"] forKey:@"banco1"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"banco2"] forKey:@"banco2"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"bola"] forKey:@"bola"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"escorregador"] forKey:@"escorregador"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"gira-gira"] forKey:@"gira-gira"];
    [itens setObject:[SKSpriteNode spriteNodeWithImageNamed:@"lixo"] forKey:@"lixo"];
    
    _char1 = [SKSpriteNode spriteNodeWithImageNamed:@"blu"];
    _char2 = [SKSpriteNode spriteNodeWithImageNamed:@"nina"];
    _char3 = [SKSpriteNode spriteNodeWithImageNamed:@"personagem1"];
    _char4 = [SKSpriteNode spriteNodeWithImageNamed:@"personagem2"];
    _char5 = [SKSpriteNode spriteNodeWithImageNamed:@"personagem3"];
    
    [chars setObject:_char1 forKey:@"blu"];
    [chars setObject:_char2 forKey:@"nina"];
    [chars setObject:_char3 forKey:@"personagem1"];
    [chars setObject:_char4 forKey:@"personagem2"];
    [chars setObject:_char5 forKey:@"personagem3"];
    
    for(int i=0;i<12;i++)
    {
        NSArray *keys;
        int r;
        CGPoint position;
        SKSpriteNode *ref;
        
        bool drawChar = ([chars count] > 0 && [self doesArray:positions has:i]);
        
        if(i == 0 || i == 3 || i == 6 || i == 9)
        {
            //altura 1
            keys = [itens allKeys];
            r = arc4random()%[itens count];
            position = CGPointMake(200 + (350 * (i/4)) +( arc4random()%200) , 100 + arc4random()%30);
            ref = [itens objectForKey:[keys objectAtIndex:r]];
            position.x -= ref.size.height/2;
            
            if(drawChar)
            {
                NSArray *keys = [chars allKeys];
                int r = arc4random()%[keys count];
                SKSpriteNode *character = [chars objectForKey:[keys objectAtIndex:r]];
                character.position = position;
                character.zPosition = -2;
                [self addChild:character];
                [chars removeObjectForKey:[keys objectAtIndex:r]];
            }
            
            ref.position = position;
            ref.zPosition = -1;
            [self addChild:ref];
            [itens removeObjectForKey:[keys objectAtIndex:r]];
        }
        if( i == 1 || i == 4 || i == 7 || i == 10)
        {
            //altura 2
            keys = [itens allKeys];
            r = arc4random()%[itens count];
            position = CGPointMake(200 + (350 * (i/4)) +( arc4random()%200) , 300 + arc4random()%30);
            ref = [itens objectForKey:[keys objectAtIndex:r]];
            position.x -= ref.size.height/2;
            
            if(drawChar)
            {
                NSArray *keys = [chars allKeys];
                int r = arc4random()%[keys count];
                SKSpriteNode *character = [chars objectForKey:[keys objectAtIndex:r]];
                character.position = position;
                character.zPosition = -4;
                [self addChild:character];
                [chars removeObjectForKey:[keys objectAtIndex:r]];
            }
            
            ref.position = position;
            ref.zPosition = -3;
            [self addChild:ref];
            [itens removeObjectForKey:[keys objectAtIndex:r]];
        }
        if(i == 2 || i == 5 || i == 8 || i == 11)
        {
            //altura 3
            keys = [itens allKeys];
            r = arc4random()%[itens count];
            position = CGPointMake(200 + (350 * (i/4)) +( arc4random()%200) , 500 + arc4random()%30);
            ref = [itens objectForKey:[keys objectAtIndex:r]];
            position.x -= ref.size.height/2;
            
            if(drawChar)
            {
                NSArray *keys = [chars allKeys];
                int r = arc4random()%[keys count];
                SKSpriteNode *character = [chars objectForKey:[keys objectAtIndex:r]];
                character.position = position;
                character.zPosition = -6;
                [self addChild:character];
                [chars removeObjectForKey:[keys objectAtIndex:r]];
            }
            
            ref.position = position;
            ref.zPosition = -5;
            [self addChild:ref];
            [itens removeObjectForKey:[keys objectAtIndex:r]];
        }
    }
    
}

-(void)tocaPodeVirar
{
    _estado = 1;
    [_rect runAction:[SKAction fadeAlphaTo:0.0f duration:2.0f]];
    [self runAction:[SKAction playSoundFileNamed:@"esconde07.m4a" waitForCompletion:NO]];
}

-(bool)doesArray:(NSArray *)array has:(int)num
{
    for(int i=0;i<[array count];i++)
    {
        NSNumber *value = [array objectAtIndex:i];
        if([value intValue] == num)
            return true;
    }
    return false;
}

@end